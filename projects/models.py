from django.db import models

from authentication.models import Account
from employer.models import Employer


class Project(models.Model):
    author = models.ForeignKey(Account)
    name = models.CharField(max_length=40, blank=True)
    descr = models.TextField(blank=True, null=True, default="")

    employer = models.ForeignKey(Employer, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    done = models.BooleanField(default=False)

    version = models.IntegerField(default=0)

    def __unicode__(self):
        return '{0}'.format(self.content)