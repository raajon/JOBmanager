from rest_framework import permissions


class IsAuthorOfProject(permissions.BasePermission):
    def has_object_permission(self, request, view, project):
        if request.user:
            return project.author == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, project):
        if request.user.is_authenticated():
            return project == request.user.username
        return False