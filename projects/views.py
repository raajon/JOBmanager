from rest_framework import permissions, viewsets
from rest_framework.response import Response

from authentication.models import Account
from projects.models import Project
from projects.permissions import IsAuthorOfProject, IsAutorized
from projects.serializers import ProjectSerializer, LogProjectSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.order_by('-created_at')
    serializer_class = ProjectSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfProject(),)

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        instance.author.version = instance.author.version+1
        instance.author.save()
        return super(ProjectViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = serializer.save()
        print instance.author.version
        instance.author.version = instance.author.version+1
        instance.author.save()


class AccountProjectsViewSet(viewsets.ViewSet):
    queryset = Project.objects.select_related('author').all()
    serializer_class = ProjectSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username, done=False)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)

class AccountLogProjectViewSet(viewsets.ViewSet):
    queryset = Project.objects.select_related('author').all()
    serializer_class = LogProjectSerializer
    permission_classes = (IsAutorized,)

    def retrieve(self, request, pk=None, account_username=None):
        data = pk.split('-')
        self.check_object_permissions(request, account_username)
        if len(data)==2:
            queryset = self.queryset.filter(author__username=account_username, pk=data[0]).exclude(version=data[1])
        else:
            queryset = self.queryset.filter(author__username=account_username, pk=data[0])
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)