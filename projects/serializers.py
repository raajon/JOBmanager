from rest_framework import serializers

from clients.serializers import ClientSerializer
from employer.serializers import EmployerSerializer
from projects.models import Project
from tasks.serializers import TaskSerializer
from tracks.models import Track
# from tracks.serializers import TrackSerializerLog
# from tracks import models


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project

        fields = ('id', 'name', 'descr', 'employer', 'created_at', 'updated_at', 'done')
        read_only_fields = ('id', 'created_at', 'updated_at')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(ProjectSerializer, self).get_validation_exclusions()

        return exclusions + ['author']

class TrackSerializerLog(serializers.ModelSerializer):
    employer = EmployerSerializer(read_only=True, required=False)
    task = TaskSerializer(read_only=True, required=False)
    client = ClientSerializer(read_only=True, required=False)

    class Meta:
        model = Track

        fields = ('id', 'employer', 'task', 'client', 'descr', 'started_at', 'stoped_at')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(TrackSerializerLog, self).get_validation_exclusions()

        return exclusions + ['author']

class LogProjectSerializer(serializers.ModelSerializer):
    track_set = TrackSerializerLog(many=True, read_only=True, required=False)

    class Meta:
        model = Project

        fields = ('id', 'name', 'descr', 'track_set', 'version')
        depth = 0;
        read_only_fields = ('id', 'created_at', 'updated_at')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(LogProjectSerializer, self).get_validation_exclusions()

        return exclusions + ['author']