from rest_framework import serializers

from clients.models import Client


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client

        fields = ('id', 'project', 'name', 'phone_number', 'email', 'address', 'descryption')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(ClientSerializer, self).get_validation_exclusions()

        return exclusions + ['author']