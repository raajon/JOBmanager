from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from authentication.models import Account
from projects.models import Project


class Client(models.Model):
    author = models.ForeignKey(Account)
    name = models.CharField(max_length=80, blank=True)
    phone_number = PhoneNumberField(blank=True, default='')
    email = models.EmailField(blank=True, default='')
    address = models.CharField(max_length=120, blank=True, default='')
    descryption = models.CharField(max_length=240, blank=True, default='')
    project = models.ManyToManyField(Project)

    def __unicode__(self):
        return '{0}'.format(self.content)