from rest_framework import permissions, viewsets
from rest_framework.response import Response

from clients.models import Client
from clients.permissions import IsAuthorOfClient, IsAutorized
from clients.serializers import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.order_by('-created_at')
    serializer_class = ClientSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfClient(),)

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        instance.author.version = instance.author.version+1
        instance.author.save()
        return super(ClientViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = serializer.save()
        print instance.author.version
        instance.author.version = instance.author.version+1
        instance.author.save()

class AccountClientsViewSet(viewsets.ViewSet):
    queryset = Client.objects.select_related('author').all()
    serializer_class = ClientSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)
