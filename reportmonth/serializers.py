from rest_framework import serializers

from reportday.serializers import DaySerializer
from reportmonth.models import Month
from tracks.serializers import TrackSerializerDay


class MonthSerializer(serializers.ModelSerializer):
    track_set = TrackSerializerDay(many=True, read_only=True, required=False)
    day_set = DaySerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Month

        fields = ('id', 'year', 'month', 'day_set', 'track_set', 'duration', 'version')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(MonthSerializer, self).get_validation_exclusions()

        return exclusions + ['author']

class MonthSerializerDay(serializers.ModelSerializer):
    class Meta:
        model = Month

        fields = ('id', 'year', 'month', 'duration', 'version')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(MonthSerializer, self).get_validation_exclusions()

        return exclusions + ['author']