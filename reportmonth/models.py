from django.db import models

from authentication.models import Account


class Month(models.Model):
    author = models.ForeignKey(Account)
    year = models.IntegerField()
    month = models.IntegerField()
    duration = models.IntegerField(default=0)
    version = models.IntegerField(default=0)

    def __unicode__(self):
        return '{0}'.format(self.content)