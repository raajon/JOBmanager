from rest_framework import permissions


class IsAuthorOfMonth(permissions.BasePermission):
    def has_object_permission(self, request, view, month):
        if request.user:
            return month.author == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, month):
        if request.user.is_authenticated():
            return month == request.user.username
        return False