from rest_framework import permissions, viewsets
from rest_framework.response import Response

from reportmonth.models import Month
from reportmonth.permissions import IsAuthorOfMonth, IsAutorized
from reportmonth.serializers import MonthSerializer


class MonthViewSet(viewsets.ModelViewSet):
    queryset = Month.objects.order_by('-created_at')
    serializer_class = MonthSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfMonth(),)

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        return super(MonthViewSet, self).perform_create(serializer)

class AccountMonthViewSet(viewsets.ViewSet):
    queryset = Month.objects.select_related('author').all()
    serializer_class = MonthSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)


    def retrieve(self, request, pk=None, account_username=None):
        self.check_object_permissions(request, account_username)
        date=pk.split('-')
        if len(date) == 3:
            queryset = self.queryset.filter(author__username=account_username,
                                            year=date[0],
                                            month=date[1]).exclude(version=date[2])

        elif len(date) == 2:
            queryset = self.queryset.filter(author__username=account_username,
                                            year=date[0],
                                            month=date[1])
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)