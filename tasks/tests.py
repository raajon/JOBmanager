from django.test import TestCase
from django.test import Client
from rest_framework.compat import RequestFactory
from rest_framework.test import APIClient

from authentication.models import Account
from tasks.models import Task
from tasks.views import TaskViewSet


class SimpleTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.c = APIClient()
        self.user1 = Account.objects.create_user(username='user1', email='user1@jomanager.pl', password='user1_pass')
        self.user2 = Account.objects.create_user(username='user2', email='user2@jomanager.pl', password='user2_pass')

        self.task1 = Task.objects.create(name="Task1", author_id=self.user1.id)
        self.task2 = Task.objects.create(name="Task2", author_id=self.user2.id)

    def test_details(self):
        # Create an instance of a GET request.

        response = self.c.get('/pl/api/v1/accounts/user1/tasks/')
        self.assertEqual(response.status_code, 403)

        self.c.force_authenticate(user=self.user1)

        response = self.c.get('/pl/api/v1/accounts/user1/tasks/')
        self.assertEqual(response.status_code, 200)

        response = self.c.get('/pl/api/v1/accounts/user2/tasks/')
        self.assertEqual(response.status_code, 403)
