from rest_framework import serializers

from tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task

        fields = ('id', 'project', 'name', 'descr')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(TaskSerializer, self).get_validation_exclusions()

        return exclusions + ['author']