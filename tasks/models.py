from django.db import models

from authentication.models import Account
from projects.models import Project


class Task(models.Model):
    author = models.ForeignKey(Account)
    name = models.CharField(max_length=40, blank=True)
    project = models.ManyToManyField(Project)
    descr = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return '{0}'.format(self.content)