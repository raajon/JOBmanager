from rest_framework import permissions, viewsets
from rest_framework.response import Response

from projects.models import Project
from tasks.models import Task
from tasks.permissions import IsAuthorOfTask, IsAutorized
from tasks.serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.order_by('-created_at')
    serializer_class = TaskSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfTask(),)

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        instance.author.version = instance.author.version+1
        instance.author.save()
        return super(TaskViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = serializer.save()
        print instance.author.version
        instance.author.version = instance.author.version+1
        instance.author.save()

class AccountTasksViewSet(viewsets.ViewSet):
    queryset = Task.objects.select_related('author').all()
    serializer_class = TaskSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)
