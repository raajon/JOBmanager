from rest_framework import permissions


class IsAuthorOfTask(permissions.BasePermission):

    def has_object_permission(self, request, view, task):
        if request.user:
            return task.author == request.user
        return False

class IsAutorized (permissions.BasePermission):

    def has_object_permission(self, request, view, task):
        if request.user.is_authenticated():
            return task == request.user.username
        return False