# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-06-20 20:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: static/templates/navbar.html:19
msgid "Track"
msgstr "Track"

#: static/templates/navbar.html:20
msgid "Todo"
msgstr "To do list"

#: static/templates/navbar.html:27
msgid "Report"
msgstr "Report"

#: static/templates/navbar.html:31
msgid "Stats"
msgstr "Statistics"

#: static/templates/navbar.html:32
msgid "DayLog"
msgstr "Day log"

#: static/templates/navbar.html:33
msgid "TrackLog"
msgstr "Track log"

#: static/templates/navbar.html:40
msgid "Manage"
msgstr "Manage"

#: static/templates/navbar.html:44
msgid "Employers"
msgstr "Employers"

#: static/templates/navbar.html:45
msgid "Projects"
msgstr "Projects"

#: static/templates/navbar.html:46
msgid "Tasks"
msgstr "Tasks"

#: static/templates/navbar.html:47
msgid "Clients"
msgstr "Clients"

#: static/templates/navbar.html:54
msgid "Profile"
msgstr "Profile"

#: static/templates/navbar.html:55
msgid "Help"
msgstr "Help"

#: static/templates/navbar.html:56
msgid "Logout"
msgstr "Logout"

#: static/templates/navbar.html:81
msgid "HOME"
msgstr "HOME"

#: static/templates/navbar.html:82
msgid "Features"
msgstr "Features"

#: static/templates/navbar.html:84
msgid "Language"
msgstr "Language"

#: static/templates/navbar.html:90
msgid "Login"
msgstr "Login"

#: static/templates/navbar.html:91
msgid "Register"
msgstr "Register"

#~ msgid "Log"
#~ msgstr "Log"
