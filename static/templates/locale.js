document.write(gettext('loading'));

document.write(gettext('btn.add'));
document.write(gettext('btn.save'));
document.write(gettext('btn.cancel'));
document.write(gettext('btn.register'));
document.write(gettext('btn.login'));
document.write(gettext('btn.confirm'));
document.write(gettext('btn.undo'));

document.write(gettext('home.header.1'));
document.write(gettext('home.header.2'));
document.write(gettext('home.track.title'));
document.write(gettext('home.track.descr'));
document.write(gettext('home.todos.title'));
document.write(gettext('home.todos.descr'));
document.write(gettext('home.reports.title'));
document.write(gettext('home.reports.descr'));
document.write(gettext('home.responsive.title'));
document.write(gettext('home.responsive.descr'));
document.write(gettext('home.final'));
document.write(gettext('home.footer.left'));
document.write(gettext('home.footer.right'));

document.write(gettext('field.params'));
document.write(gettext('field.name'));
document.write(gettext('field.descr'));
document.write(gettext('field.phone'));
document.write(gettext('field.email'));
document.write(gettext('field.address'));
document.write(gettext('field.date'));
document.write(gettext('field.time'));
document.write(gettext('field.duration'));
document.write(gettext('field.startTime'));
document.write(gettext('field.stopTime'));
document.write(gettext('field.from'));
document.write(gettext('field.to'));
document.write(gettext('field.nextDay'));
document.write(gettext('field.total'));
document.write(gettext('field.default'));
document.write(gettext('field.other'));
document.write(gettext('field.today'));
document.write(gettext('field.close'));
document.write(gettext('field.prio'));
document.write(gettext('field.prio.1'));
document.write(gettext('field.prio.2'));
document.write(gettext('field.prio.3'));
document.write(gettext('field.prio.4'));
document.write(gettext('field.prio.5'));
document.write(gettext('field.expiredTime'));
document.write(gettext('field.passremind'));

document.write(gettext('field.noTracks.title'));
document.write(gettext('field.noEmployers.title'));
document.write(gettext('field.noProjects.title'));
document.write(gettext('field.noTasks.title'));
document.write(gettext('field.noClients.title'));
document.write(gettext('field.noTodos.title'));

document.write(gettext('field.noTracks.descr'));
document.write(gettext('field.noEmployers.descr'));
document.write(gettext('field.noProjects.descr'));
document.write(gettext('field.noTasks.descr'));
document.write(gettext('field.noClients.descr'));
document.write(gettext('field.noTodos.descr'));

document.write(gettext('field.day.freeday'));
document.write(gettext('field.day.hollyday'));
document.write(gettext('field.day.workdDegree'));
document.write(gettext('field.day.workdDegree.0'));
document.write(gettext('field.day.workdDegree.1'));
document.write(gettext('field.day.workdDegree.2'));
document.write(gettext('field.day.workdDegree.3'));
document.write(gettext('field.day.dayDegree'));
document.write(gettext('field.day.dayDegree.0'));
document.write(gettext('field.day.dayDegree.1'));
document.write(gettext('field.day.dayDegree.2'));
document.write(gettext('field.day.dayDegree.3'));

document.write(gettext('field.confirm'));
document.write(gettext('field.login'));
document.write(gettext('field.register'));
document.write(gettext('field.remind'));
document.write(gettext('field.email'));
document.write(gettext('field.username'));
document.write(gettext('field.firstname'));
document.write(gettext('field.lastname'));
document.write(gettext('field.pass'));
document.write(gettext('field.pass2'));
document.write(gettext('field.login'));

document.write(gettext('kind.task'));
document.write(gettext('kind.tasks'));
document.write(gettext('kind.client'));
document.write(gettext('kind.clients'));
document.write(gettext('kind.project'));
document.write(gettext('kind.projects'));
document.write(gettext('kind.employer'));
document.write(gettext('kind.employers'));
document.write(gettext('kind.track'));
document.write(gettext('kind.log'));
document.write(gettext('kind.todo'));
document.write(gettext('kind.tracklog'));
document.write(gettext('kind.daylog'));
document.write(gettext('kind.statslog'));
document.write(gettext('kind.help'));
document.write(gettext('kind.profile'));

document.write(gettext('kind.report'));
document.write(gettext('kind.manage'));
document.write(gettext('kind.logout'));
document.write(gettext('kind.home'));
document.write(gettext('kind.features'));

document.write(gettext('kind.project.current'));
document.write(gettext('kind.project.done'));

document.write(gettext('msg.registerSucces.title'));
document.write(gettext('msg.registerSucces.desc'));
document.write(gettext('msg.registerError.title'));
document.write(gettext('msg.registerError.desc'));
document.write(gettext('msg.confirmSucces.title'));
document.write(gettext('msg.confirmSucces.desc'));
document.write(gettext('msg.confirmError.title'));
document.write(gettext('msg.confirmError.desc'));


document.write(gettext('msg.log.error'));
document.write(gettext('msg.trackstop.success'));
document.write(gettext('msg.trackstop.error'));
document.write(gettext('msg.trackcreate.success'));
document.write(gettext('msg.trackcreate.error'));
document.write(gettext('msg.trackupdated.success'));
document.write(gettext('msg.trackupdated.error'));
document.write(gettext('msg.trackremove.success'));
document.write(gettext('msg.trackremove.error'));
document.write(gettext('msg.dayupdated.success'));
document.write(gettext('msg.dayupdated.error'));

document.write(gettext('msg.employercreated.success'));
document.write(gettext('msg.employercreated.error'));
document.write(gettext('msg.employerupdated.success'));
document.write(gettext('msg.employerupdated.error'));

document.write(gettext('msg.projectcreated.success'));
document.write(gettext('msg.projectcreated.error'));
document.write(gettext('msg.projectupdated.success'));
document.write(gettext('msg.projectupdated.error'));

document.write(gettext('msg.taskcreated.success'));
document.write(gettext('msg.taskcreated.error'));
document.write(gettext('msg.taskupdated.success'));
document.write(gettext('msg.taskupdated.error'));

document.write(gettext('msg.clientcreated.success'));
document.write(gettext('msg.clientcreated.error'));
document.write(gettext('msg.clientupdated.success'));
document.write(gettext('msg.clientupdated.error'));

document.write(gettext('msg.todocreate.success'));
document.write(gettext('msg.todocreate.error'));
document.write(gettext('msg.todoupdated.success'));
document.write(gettext('msg.todoupdated.error'));

document.write(gettext('msg.profileupdated.success'));
document.write(gettext('msg.profileupdated.error'));

document.write(gettext('msg.log.tracks.empty'));

document.write(gettext('msg.passremind.err'));
document.write(gettext('msg.passremind.invalidKey'));
document.write(gettext('msg.passremind.expired'));
document.write(gettext('msg.passremind.success'));

document.write(gettext('help.employers.desc'));
document.write(gettext('help.employers.desc.0'));
document.write(gettext('help.employers.desc.1'));
document.write(gettext('help.employers.desc.2'));
document.write(gettext('help.employers.desc.3'));
document.write(gettext('help.employers.desc.4'));
document.write(gettext('help.employers.desc.5'));
document.write(gettext('help.employers.desc.6'));
document.write(gettext('help.employers.desc.7'));
document.write(gettext('help.employers.desc.8'));
document.write(gettext('help.employers.desc.9'));
document.write(gettext('help.employers.desc.10'));

document.write(gettext('help.projects.desc'));
document.write(gettext('help.projects.desc.0'));
document.write(gettext('help.projects.desc.1'));
document.write(gettext('help.projects.desc.2'));
document.write(gettext('help.projects.desc.3'));
document.write(gettext('help.projects.desc.4'));
document.write(gettext('help.projects.desc.5'));
document.write(gettext('help.projects.desc.6'));
document.write(gettext('help.projects.desc.7'));
document.write(gettext('help.projects.desc.8'));
document.write(gettext('help.projects.desc.9'));
document.write(gettext('help.projects.desc.10'));

document.write(gettext('help.tasks.desc'));
document.write(gettext('help.tasks.desc.0'));
document.write(gettext('help.tasks.desc.1'));
document.write(gettext('help.tasks.desc.2'));
document.write(gettext('help.tasks.desc.3'));
document.write(gettext('help.tasks.desc.4'));
document.write(gettext('help.tasks.desc.5'));
document.write(gettext('help.tasks.desc.6'));
document.write(gettext('help.tasks.desc.7'));
document.write(gettext('help.tasks.desc.8'));
document.write(gettext('help.tasks.desc.9'));
document.write(gettext('help.tasks.desc.10'));

document.write(gettext('help.clients.desc'));
document.write(gettext('help.clients.desc.0'));
document.write(gettext('help.clients.desc.1'));
document.write(gettext('help.clients.desc.2'));
document.write(gettext('help.clients.desc.3'));
document.write(gettext('help.clients.desc.4'));
document.write(gettext('help.clients.desc.5'));
document.write(gettext('help.clients.desc.6'));
document.write(gettext('help.clients.desc.7'));
document.write(gettext('help.clients.desc.8'));
document.write(gettext('help.clients.desc.9'));
document.write(gettext('help.clients.desc.10'));

document.write(gettext('help.tracks.desc'));
document.write(gettext('help.tracks.desc.0'));
document.write(gettext('help.tracks.desc.1'));
document.write(gettext('help.tracks.desc.2'));
document.write(gettext('help.tracks.desc.3'));
document.write(gettext('help.tracks.desc.4'));
document.write(gettext('help.tracks.desc.5'));
document.write(gettext('help.tracks.desc.6'));
document.write(gettext('help.tracks.desc.7'));
document.write(gettext('help.tracks.desc.8'));
document.write(gettext('help.tracks.desc.9'));
document.write(gettext('help.tracks.desc.10'));
