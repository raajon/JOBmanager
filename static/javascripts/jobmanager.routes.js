(function () {
  'use strict';

  angular
    .module('jobmanager.routes')
    .config(config);

  config.$inject = ['$routeProvider'];

  /**
  * @name config
  * @desc Define valid application routes
  */
  function config($routeProvider) {
    $routeProvider.when('/:language/register', {
        controller: 'LoginController',
        controllerAs: 'vm',
        templateUrl: '/static/templates/authentication/login.html'
    }).when('/:language/login', {
        controller: 'LoginController',
        controllerAs: 'vm',
        templateUrl: '/static/templates/authentication/login.html'
    }).when('/:language/remind', {
        controller: 'LoginController',
        controllerAs: 'vm',
        templateUrl: '/static/templates/authentication/login.html'
    }).when('/:language/confirm/:key', {
        controller: 'ConfirmController',
        controllerAs: 'vm',
        templateUrl: "static/templates/authentication/confirm.html"
    }).when('/:language/changepass/:key', {
        controller: 'ResetController',
        controllerAs: 'vm',
        templateUrl: "static/templates/authentication/reset.html"
    }).when('/:language/profile', {
        controller: 'ProfileController',
        controllerAs: 'vm',
        templateUrl: '/static/templates/manage/profile/profile.html'
    }).when('/:language/tracks', {
        controller: 'TracksController',
        controllerAs: 'vm',
        templateUrl: "static/templates/tracks/tracks.html"
    }).when('/:language/tracks/:date', {
        controller: 'TracksController',
        controllerAs: 'vm',
        templateUrl: "static/templates/tracks/tracks.html"
    }).when('/:language/todo', {
        controller: 'TodosController',
        controllerAs: 'vm',
        templateUrl: "static/templates/todos/todos.html"
    }).when('/:language/log/:kind', {
        controller: 'LogController',
        controllerAs: 'vm',
        templateUrl: "static/templates/report/log/log.html"
    }).when('/:language/log/:kind/:dateFrom/:dateTo', {
        controller: 'LogController',
        controllerAs: 'vm',
        templateUrl: "static/templates/report/log/log.html"
    }).when('/:language/logproject', {
        controller: 'LogprojectController',
        controllerAs: 'vm',
        templateUrl: "static/templates/report/logproject/logproject.html"
    }).when('/:language/logproject/:project', {
        controller: 'LogprojectController',
        controllerAs: 'vm',
        templateUrl: "static/templates/report/logproject/logproject.html"
    }).when('/:language/employers', {
        controller: 'EmployersController',
        controllerAs: 'vm',
        templateUrl: "static/templates/manage/employers/employers.html"
    }).when('/:language/projects', {
        controller: 'ProjectsController',
        controllerAs: 'vm',
        templateUrl: "static/templates/manage/projects/projects.html"
    }).when('/:language/tasks', {
        controller: 'TasksController',
        controllerAs: 'vm',
        templateUrl: "static/templates/manage/tasks/tasks.html"
    }).when('/:language/clients', {
        controller: 'ClientsController',
        controllerAs: 'vm',
        templateUrl: "static/templates/manage/clients/clients.html"
    }).when('/:language/help', {
        controller: 'HelpController',
        controllerAs: 'vm',
        templateUrl: "static/templates/help/help.html"
    }).when('/:language/', {
        controller: 'HomeController',
        controllerAs: 'vm',
        templateUrl: "static/templates/home/home.html"
    }).otherwise('/:language/');
  }
})();