/**
* LoginController
* @namespace jobmanager.tracks.controllers
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.tracks.controllers')
    .controller('TracksController', TracksController);

  TracksController.$inject = ['$location', '$route', '$scope', '$interval', '$locale', '$filter', 'Authentication', 'Day', 'Tracks', 'Manage'];

  /**
  * @namespace TracksController
  */
  function TracksController($location, $route, $scope, $interval, $locale, $filter, Authentication, Day, Tracks, Manage) {

    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.path=$route.current.params
    vm.day={
        date: vm.path.date?new Date(vm.path.date): new Date(),
        track_set: []
    };
    vm.day.date.setUTCHours(0);
    vm.day.date.setUTCMinutes(0);
    vm.day.date.setUTCSeconds(0);
    vm.day.date.setUTCMilliseconds(0);

    activate();
    vm.clock = Date.now();
    vm.day.track_set= [];
    vm.employers= [];
    vm.projects= [];
    vm.tasks= [];
    vm.clients= [];

    vm.locale = $locale.DATETIME_FORMATS;
    $scope.today = $filter('i18n')("field.today");
    $scope.clear = '';
    $scope.close = $filter('i18n')("field.close");


    vm.prevDay = function(){
        vm.day.date.setDate(vm.day.date.getDate() - 1);
        $location.url('/' + vm.path.language + '/tracks/' +  moment(vm.day.date).format('YYYY-MM-DD'));
        getDay();
    };
    vm.nextDay = function(){
        vm.day.date.setDate(vm.day.date.getDate() + 1);
        $location.url('/' + vm.path.language + '/tracks/' +  moment(vm.day.date).format('YYYY-MM-DD'));
        getDay();
    };
    vm.setDay = function () {
        $location.url('/' + vm.path.language + '/tracks/' +  moment(vm.day.date).format('YYYY-MM-DD'));
        getDay();
    };
    vm.newTrack = {
        employer : {},
        project : {},
        task : {},
        client : {},
        descr: "",
        started_at: null,
        stoped_at : null
    };
    vm.removeTrack = function(track){
        Tracks.destroy(track).then(
            function(data, status, headers, config) {
                vm.day.track_set =_.without(vm.day.track_set, track);
                Materialize.toast($filter('i18n')("msg.trackremove.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.trackremove.error"),2000, 'error-toast');
            }
        );
    }
    vm.restartTrack = function(track){
        vm.stopTracks();
        var newTrack = {
            id : track.id,
            employer : track.employer,
            project : track.project,
            descr : track.descr,
            task : track.task,
            client :track.client,
            started_at: new Date(),
            stoped_at : null,
//            month: vm.day.month.id
        };
        vm.saveTrack(newTrack);
    }
    vm.stopTrack = function(track){
        track.stoped_at = new Date();
        Tracks.update(track).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.trackstop.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.trackstop.error"),2000, 'error-toast');
            }
        );
    }
    vm.stopTracks = function(){
        _.forEach(vm.day.track_set, function(track){
            if(!track.stoped_at){
                track.stoped_at = new Date();
                Tracks.update(track).then(
                    function(data, status, headers, config) {
                        Materialize.toast($filter('i18n')("msg.trackstop.success"),2000);
                    },
                    function(data, status, headers, config) {
                        console.log(data.error);
                        Materialize.toast($filter('i18n')("msg.trackstop.error"),2000, 'error-toast');
                    }
                );
            }
        });
    }
    vm.editTrack = function(track){
        vm.clear();
        vm.add=false;
        vm.edit=true;
        var stoped_at = null
        if(track.stoped_at){
            stoped_at = new Date(track.stoped_at);
        }
        vm.newTrack = {
            id : track.id,
            employer : track.employer,
            project: track.project,
            task: track.task,
            client: track.client,
            descr : track.descr,
            started_at: new Date(track.started_at),
            stoped_at : stoped_at,
            nextDay: new Date(track.stoped_at).getDate() != new Date(track.started_at).getDate(),
//            day: track.day.id,
//            month: track.month.id
        };
    }
    vm.newTrackFn = function(){
        vm.clear();
        vm.add=true;
        vm.edit=false;
        var stoped_at = null;
    }
    vm.getDiff = function(date1, date2){
        if (!date1){date1=Date.now();}
        var diff = (new Date(date1).getTime()- new Date(date2).getTime());
        return new Date(diff);
    }
    vm.saveTrack = function(newTrack) {
        vm.stopTracks();
        var day = new Date(vm.day.date);
        if(!newTrack.started_at){
            newTrack.started_at =new Date();
        }else{
            newTrack.started_at.setYear(day.getFullYear());
            newTrack.started_at.setMonth(day.getMonth());
            newTrack.started_at.setDate(day.getDate());
        }
        if(newTrack.stoped_at){
            newTrack.stoped_at.setYear(day.getFullYear());
            newTrack.stoped_at.setMonth(day.getMonth());
            if(newTrack.nextDay){
                newTrack.stoped_at.setDate(day.getDate()+1);
            }else{
                newTrack.stoped_at.setDate(day.getDate());
            }
        };
        newTrack.day=vm.day;
        newTrack.month=vm.day.month;
        vm.day.track_set.push(newTrack);
        vm.add=false;
        Tracks.create(newTrack).then(
            function createTracksuccessFn(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.trackcreate.success"),2000);
                newTrack = {};
                getDay();
            },function createTrackErrorFn(data, status, headers, config) {
                vm.day.track_set.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.trackcreate.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateTrack = function(newTrack){
        var day = new Date(vm.day.date);
        if(newTrack.stoped_at){
            newTrack.stoped_at.setYear(day.getFullYear());
            newTrack.stoped_at.setMonth(day.getMonth());
            if(newTrack.nextDay){
                newTrack.stoped_at.setDate(day.getDate()+1);
            }else{
                newTrack.stoped_at.setDate(day.getDate());
            }
        };
        Tracks.update(newTrack).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.trackupdated.success"),2000);
                vm.newTrack = {};
                getDay();
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.trackupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateDay = function(){
        var dayToUpdate = _.clone(vm.day);
        dayToUpdate.date = moment(vm.day.date).format('YYYY-MM-DD');
        Day.update(dayToUpdate).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.dayupdated.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.dayupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.getTotalDuration = function(){
        var total = 0;
        _.forEach(vm.day.track_set, function(track){
            if (track.stoped_at){
                total= total + (new Date(track.stoped_at).getTime()-new Date(track.started_at).getTime());
            }else{
                total= total + (Date.now()-new Date(track.started_at).getTime());
            }
        });
        return total;
    }

    function activate() {
        getDay();

        Manage.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.employers = data.employer_set;
                vm.projects = data.project_set;
                vm.projects = _.where(vm.projects, {done: false})
                vm.tasks = data.task_set;
                vm.clients = data.client_set;
                vm.clear();
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    function getDay(){
        var formattedDate = moment(vm.day.date).format('YYYY-MM-DD');
        Day.get(Authentication.getAuthenticatedAccount().username, formattedDate).then(
            function(data, status, headers, config) {
                if(data.data.length==0){
                    console.log(data.error);
                }else{
                    vm.day=data.data[0];
                    vm.day.date = new Date(vm.day.date);
                    vm.newTrack.month=vm.day.month.id;
                }
            },
            function(data, status, headers, config) {
                console.log(data.error);
                if (data.status ==403){
                    Authentication.logout();
                    Authentication.unauthenticate();
                }
            }
        );
    }

    vm.clear = function(){
        vm.newTrack.employer = _.findWhere(vm.employers, {'default': true});
        vm.newTrack.project = null;
        vm.newTrack.task = null;
        vm.newTrack.client = null;
        vm.newTrack.descr = "";
        vm.newTrack.started_at = null;
        vm.newTrack.stoped_at = null;
        _.forEach(vm.day.track_set, function(track){
            track.edit=false;
        });
    }

    var tick = function() {
        vm.clock = Date.now();
    }
    $interval(tick, 1000);
  }
})();