/**
* Tracks
* @namespace jobmanager.tracks.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.tracks.services')
    .factory('Tracks', Tracks);

  Tracks.$inject = ['$http'];

function Tracks($http) {
    var Tracks = {
        all: all,
        create: create,
        get: get,
        destroy: destroy,
        update: update
    };

    return Tracks;

    function all() {
        return $http.get('/api/v1/tracks/');
    }

    function create(track) {
        if (!track.employer){track.employer={}}
        if (!track.project){track.project={}}
        if (!track.task){track.task={}}
        if (!track.client){track.client={}}
        return $http.post('/en/api/v1/tracks/', {
            employer: track.employer.id,
            project: track.project.id,
            task: track.task.id,
            client: track.client.id,
            descr: track.descr,
            started_at: track.started_at,
            stoped_at: track.stoped_at,
            day: track.day.id,
            month:track.month.id
        });
    }

    function get(username, day) {
        return $http.get('/en/api/v1/accounts/' + username + '/tracks/' + day + '/');
    }

    function destroy(track){
        return $http.delete('/en/api/v1/tracks/' + track.id + '/');
    }

    function update(track) {
        var trackToUpdate = _.clone(track);

        if(track.employer && typeof track.employer === 'object'){
            trackToUpdate.employer = track.employer.id
        }
        if(track.project && typeof track.project === 'object'){
            trackToUpdate.project = track.project.id
        }
        if(track.task && typeof track.task === 'object'){
            trackToUpdate.task = track.task.id
        }
        if(track.client && typeof track.client === 'object'){
            trackToUpdate.client = track.client.id
        }
        if(typeof track.day === 'object'){
            trackToUpdate.day = track.day.id
        }
        if(typeof track.month === 'object'){
            trackToUpdate.month = track.month.id
        }
        return $http.put('/en/api/v1/tracks/' + track.id + '/', trackToUpdate);
    }
}
})();