(function () {
  'use strict';

  angular
    .module('jobmanager.tracks', [
      'jobmanager.tracks.controllers',
      'jobmanager.tracks.services'
    ]);

  angular
    .module('jobmanager.tracks.controllers', []);

  angular
    .module('jobmanager.tracks.services', ['ngCookies']);
})();