(function () {
  'use strict';

  angular
    .module('jobmanager.authentication.services')
    .factory('Authentication', Authentication);

  Authentication.$inject = ['$cookies', '$http', '$location'];

  function Authentication($cookies, $http, $location) {

    var Authentication = {
      getAuthenticatedAccount: getAuthenticatedAccount,
      isAuthenticated: isAuthenticated,
      login: login,
      logout: logout,
      register: register,
      remind: remind,
      changePass: changePass,
      setAuthenticatedAccount: setAuthenticatedAccount,
      unauthenticate: unauthenticate,
      confirm: confirm,
      get: get,
      update: update
    };

    return Authentication;

    function register(email, password, username) {
      return $http.post('/en/api/v1/accounts/', {
        username: username,
        password: password,
        email: email
      });
    }

    function remind(email) {
      return $http.post('/en/api/v1/auth/remind/', {
        email: email
      })
    }

    function changePass(key, pass) {
      return $http.post('/en/api/v1/auth/changepass/', {
        key: key,
        pass: pass
      })
    }

    function login(email, password) {
      return $http.post('/en/api/v1/auth/login/', {
        email: email, password: password
      })
    }

    function logout() {
      return $http.post('/en/api/v1/auth/logout/')
        .then(logoutSuccessFn, logoutErrorFn);

      function logoutSuccessFn(data, status, headers, config) {
        Authentication.unauthenticate();

        window.location = '/login';
      }

      function logoutErrorFn(data, status, headers, config) {
        console.error('Epic failure!');
        window.location = '/login';
      }
    }

    function getAuthenticatedAccount() {
      if (!$cookies.get("authenticatedAccount")) {
        return;
      }
      return JSON.parse($cookies.get("authenticatedAccount"));
    }

    function isAuthenticated(noNeed) {
        if(!$cookies.get("authenticatedAccount") && !noNeed){
            $location.path("/login");
        }
        return !!$cookies.get("authenticatedAccount");
    }

    function setAuthenticatedAccount(account) {
        $cookies.put("authenticatedAccount", JSON.stringify(account));
    }

    function unauthenticate() {
        $cookies.remove("authenticatedAccount");
    }

    function confirm(key) {
      return $http.get('/en/api/v1/confirm/' + key + '/');
    }

    function get(username) {
      return $http.get('/api/v1/accounts/' + username + '/');
    }

    function update(profile) {
        return $http.put('/en/api/v1/accounts/' + profile.username + '/', profile);
    }
  }
})();