(function () {
  'use strict';

  angular
    .module('jobmanager.authentication', [
      'jobmanager.authentication.controllers',
      'jobmanager.authentication.directives',
      'jobmanager.authentication.services'
    ]);

  angular
    .module('jobmanager.authentication.controllers', []);

  angular
    .module('jobmanager.authentication.directives', []);

  angular
    .module('jobmanager.authentication.services', ['ngCookies']);
})();