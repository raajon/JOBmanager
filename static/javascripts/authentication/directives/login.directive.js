(function () {
  'use strict';

  angular
    .module('jobmanager.authentication.directives')
    .directive('login', function () {
        var directive = {
          controller: 'LoginController',
          controllerAs: 'vm',
          restrict: 'A',
          scope: {
            navbar: '='
          },
          templateUrl: '/static/templates/authentication/login.html'
        };
        return directive;
  });
})();
