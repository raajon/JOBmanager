/**
* LoginController
* @namespace jobmanager.authentication.controllers
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.authentication.controllers')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$location', '$scope', '$window', 'Authentication', 'Snackbar'];

  /**
  * @namespace LoginController
  */
  function LoginController($location, $scope, $window, Authentication, Snackbar) {
    var vm = this;

    vm.login = login;
    vm.register = register;
    vm.remind = remind;
    vm.loginRegister = 0;
    vm.procesing=false;
    vm.path=$location.path().split("/")


    if (Authentication.isAuthenticated(true)) {
        $location.url('/' + vm.path[1] + '/tracks');
    }else{
        if(vm.path[2]=="register"){
            vm.loginRegister = 1;
        }else if(vm.path[2]=="remind"){
            vm.loginRegister = 2;
        }
    }

    function login() {
        vm.procesing=true;
        Authentication.login(vm.email, vm.password).then(
            function(data, status, headers, config) {
                vm.procesing=false;
                Authentication.setAuthenticatedAccount(data.data);
                $window.location.href = '/' + vm.path[1] + '/tracks';
            },
            function(data, status, headers, config) {
                vm.procesing=false;
                Snackbar.error(data.data.message);
            }
        );
    }

    function register() {
        vm.procesing=true;
        Authentication.register(vm.email, vm.password, vm.username).then(function(){
            vm.procesing=false;
            vm.registerSuccess=true;
        }, function(data){
            vm.procesing=false;
            Snackbar.error(data.data.message);
        });
    }

    function remind() {
        vm.procesing=true;
        Authentication.remind(vm.email).then(
            function(data, status, headers, config) {
                vm.procesing=false;
                vm.remindSuccess = true
            },
            function(data, status, headers, config) {
                vm.procesing=false;
                Snackbar.error(data.data.message);
            }
        );
    }

    vm.changetoLogin = function(){
        $location.url('/' + vm.path[1] + '/login');
    }

    vm.changetoRegister = function(){
        $location.url('/' + vm.path[1] + '/register');
    }

    vm.changetoRemind = function(){
        $location.url('/' + vm.path[1] + '/remind');
    }
  }
})();