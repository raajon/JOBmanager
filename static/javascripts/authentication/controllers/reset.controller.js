
(function () {
    'use strict';

    angular
        .module('jobmanager.authentication.controllers')
        .controller('ResetController', ResetController);

    ResetController.$inject = ['$location', '$scope', '$route', 'Authentication'];

    function ResetController($location, $scope, $route, Authentication) {
        var vm = this;
        vm.success = false;
        vm.error = false;
        vm.passSuccess = true;
        vm.password1 = "dupa";
        vm.password2 = "dupa";
        vm.errMsg = "";

        vm.path=$location.path().split("/")


        if (Authentication.isAuthenticated(true)) {
            $location.url('/' + vm.path[1] + '/tracks');
        }

        vm.remind = function(){
            if(vm.password1 == vm.password2 & vm.password1.length>6){
                vm.passErr = false;
                Authentication.changePass($route.current.params.key, vm.password1).then(
                    function(data){
                        vm.success = true;
                        console.log(data);
                    },function(data){
                    console.log(data);
                        if(data.status ==404){
                            vm.passErr = true;
                            vm.errMsg = 'msg.passremind.invalidKey';

                        }else{
                            vm.passErr = true;
                            vm.errMsg = data.data.message;
                        }
                    }
                )
            }else{
                vm.passErr = true;
                vm.errMsg = "msg.passremind.err"
            }
        }

        vm.changetoLogin = function(){
            $location.url('/' + vm.path[1] + '/login');
        }
    }
})();