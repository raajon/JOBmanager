
(function () {
    'use strict';

    angular
        .module('jobmanager.authentication.controllers')
        .controller('ConfirmController', ConfirmController);

    ConfirmController.$inject = ['$location', '$scope', '$route', 'Authentication'];

    function ConfirmController($location, $scope, $route, Authentication) {
        var vm = this;
        vm.success = false;
        vm.error = false;

        vm.path=$location.path().split("/")


        if (Authentication.isAuthenticated(true)) {
            $location.url('/' + vm.path[1] + '/tracks');
        }

        Authentication.confirm($route.current.params.key).then(
            function(data){
                vm.success = true;
                console.log(data);
            },function(data){
                vm.error = true
                console.log(data.statusText);
            }
        )
    }
})();