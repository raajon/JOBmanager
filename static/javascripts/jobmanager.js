angular
  .module('jobmanager', []);

(function () {
    'use strict';

    var module = angular
        .module('jobmanager', [
            'ui.materialize',
            'angularMoment',
            'angularMaterializeDatePicker',
            "angular-duration-format",
            "angular-loading-bar",
            "chart.js",
            'filters',
            'jobmanager.config',
            'jobmanager.routes',
            'jobmanager.authentication',
            'jobmanager.layout',
            'jobmanager.utils',
            'jobmanager.clients',
            'jobmanager.manage',
            'jobmanager.employers',
            'jobmanager.projects',
            'jobmanager.tasks',
            'jobmanager.todos',
            'jobmanager.tracks',
            'jobmanager.profile',
            'jobmanager.log',
            'jobmanager.logproject',
            'jobmanager.day',
            'jobmanager.month',
            'jobmanager.home',
            'jobmanager.help',
        ]);

    angular
        .module('jobmanager.routes', ['ngRoute']);

    angular
        .module('jobmanager.config', []);

    angular
        .module('jobmanager')
        .run(run);

    module.config(function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-backgroud"></div><div id="loading-modal"><div id="loading-border" class="center-align"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div></div>';

        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.latencyThreshold = 500;
    });

    run.$inject = ['$http'];

    /**
    * @name run
    * @desc Update xsrf $http headers to align with Django's defaults
    */
    function run($http) {
      $http.defaults.xsrfHeaderName = 'X-CSRFToken';
      $http.defaults.xsrfCookieName = 'csrftoken';
    }
})();