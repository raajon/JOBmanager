(function () {
  'use strict';

  angular
    .module('jobmanager.layout', [
      'jobmanager.layout.controllers',
      'jobmanager.layout.directives'
    ]);

  angular
    .module('jobmanager.layout.controllers', []);


  angular
    .module('jobmanager.layout.directives', []);
})();