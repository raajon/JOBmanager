(function () {
  'use strict';

  angular
    .module('jobmanager.layout.directives')
    .directive('navbar', function () {
        var directive = {
          controller: 'NavbarController',
          controllerAs: 'vm',
          restrict: 'A',
          scope: {
            navbar: '='
          },
          templateUrl: '/static/templates/layout/navbar.html'
        };
        return directive;
  });
})();