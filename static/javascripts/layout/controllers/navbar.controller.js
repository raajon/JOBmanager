/**
* NavbarController
* @namespace jobmanager.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.layout.controllers')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$scope', '$location', 'Authentication'];

  /**
  * @namespace NavbarController
  */
  function NavbarController($scope, $location, Authentication) {
    var vm = this;
    vm.logout = logout;
    vm.path=$location.path().split("/")
    vm.lang = vm.path[1];
console.log(vm.lang);

    if (Authentication.isAuthenticated(true)) {
        vm.user = Authentication.getAuthenticatedAccount();
        if(vm.path[2]==""){
            $location.url('/' + vm.path[1] + '/tracks');
        }
        vm.login=true;
    }else{
        vm.login=false;
    }


    function logout() {
      Authentication.logout();
    }
  }
})();