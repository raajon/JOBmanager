/**
* LoginController
* @namespace jobmanager.todos.controllers
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.todos.controllers')
    .controller('TodosController', TodosController);

  TodosController.$inject = ['$location', '$route', '$scope', '$interval', '$locale', '$filter', 'Authentication', 'Day', 'Todos', 'Tracks', 'Manage'];

  /**
  * @namespace TodosController
  */
  function TodosController($location, $route, $scope, $interval, $locale, $filter, Authentication, Day, Todos, Tracks, Manage) {

    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.path=$route.current.params
    vm.day={
        date: vm.path.date?new Date(vm.path.date): new Date(),
        todo_set: []
    };
    vm.day.date.setUTCHours(0);
    vm.day.date.setUTCMinutes(0);
    vm.day.date.setUTCSeconds(0);
    vm.day.date.setUTCMilliseconds(0);

    activate();
    vm.clock = Date.now();
    vm.day.todo_set= [];
    vm.employers= [];
    vm.projects= [];
    vm.tasks= [];
    vm.clients= [];

    vm.locale = $locale.DATETIME_FORMATS;
    $scope.today = $filter('i18n')("field.today");
    $scope.clear = '';
    $scope.close = $filter('i18n')("field.close");

    vm.newTodo = {
        employer : {},
        project : {},
        task : {},
        client : {},
        descr: "",
        expired_at: null,
        prio : 0
    };
    vm.removeTodo = function(todo){
        Todos.destroy(todo).then(
            function(data, status, headers, config) {
                vm.day.todo_set =_.without(vm.day.todo_set, todo);
                Materialize.toast($filter('i18n')("msg.todoremove.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.todoremove.error"),2000, 'error-toast');
            }
        );
    }
    vm.restartTodo = function(todo){
        vm.stopTodos();
        var newTodo = {
            id : todo.id,
            employer : todo.employer,
            project : todo.project,
            descr : todo.descr,
            task : todo.task,
            client :todo.client,
            expired_at: new Date(),
            prio : todo.prio
        };
        vm.saveTodo(newTodo);
    }
    vm.editTodo = function(todo){
        vm.clear();
        vm.add=false;
        vm.edit=true;
        vm.newTodo = {
            id : todo.id,
            employer : todo.employer,
            project: todo.project,
            task: todo.task,
            client: todo.client,
            descr : todo.descr,
            expired_at: new Date(todo.expired_at),
            prio : todo.prio
        };
    }
    vm.newTodoFn = function(){
        vm.clear();
        vm.add=true;
        vm.edit=false;
        var expired_at = null;
    }
    vm.saveTodo = function(newTodo) {
        vm.day.todo_set.push(newTodo);
        vm.add=false;
        Todos.create(newTodo).then(
            function createTodosuccessFn(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.todocreate.success"),2000);
                newTodo = {};
                getTodos();
            },function createTodoErrorFn(data, status, headers, config) {
                vm.day.todo_set.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.todocreate.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateTodo = function(newTodo){
        Todos.update(newTodo).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.todoupdated.success"),2000);
                vm.newTodo = {};
                getTodos();
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.todoupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.doneTodo = function(todo){
        todo.done=true;
        Todos.update(todo).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.todoupdated.success"),2000);
                vm.todos =_.without(vm.todos, todo);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.todoupdated.error"),2000, 'error-toast');
            }
        );
    }

    vm.startTrack = function(todo){
        var formattedDate = moment(vm.day.date).format('YYYY-MM-DD');
        Day.get(Authentication.getAuthenticatedAccount().username, formattedDate).then(
            function(data, status, headers, config) {
                var day = data.data[0];
                _.forEach(day.track_set, function(track){
                    if(!track.stoped_at){
                        track.stoped_at = new Date();
                        Tracks.update(track).then(
                            function(data, status, headers, config) {
                                Materialize.toast($filter('i18n')("msg.trackstop.success"),2000);
                            },
                            function(data, status, headers, config) {
                                console.log(data.error);
                                Materialize.toast($filter('i18n')("msg.trackstop.error"),2000, 'error-toast');
                            }
                        );
                    }
                });
                todo.day = day;
                todo.month = day.month;
                todo.started_at =new Date();
                Tracks.create(todo).then(
                    function createTracksuccessFn(data, status, headers, config) {
                        Materialize.toast($filter('i18n')("msg.trackcreate.success"),2000);
                        vm.path=$location.path().split("/")
                        $location.url('/' + vm.path[1] + '/tracks');
                    },function createTrackErrorFn(data, status, headers, config) {
                        Materialize.toast($filter('i18n')("msg.trackcreate.error"),2000, 'error-toast');
                    }
                );
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    function activate() {
        getTodos()

        Manage.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.employers = data.employer_set;
                vm.projects = data.project_set;
                vm.tasks = data.task_set;
                vm.clients = data.client_set;
                vm.clear();
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    function getTodos(){
        Todos.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.todos = data.data;
                vm.clear();
            },
            function(data, status, headers, config) {
                console.log(data.error);
                if (data.status ==403){
                    Authentication.logout();
                    Authentication.unauthenticate();
                }
            }
        );
    }

    vm.clear = function(){
        vm.newTodo.employer = _.findWhere(vm.employers, {'default': true});
        vm.newTodo.project = null;
        vm.newTodo.task = null;
        vm.newTodo.client = null;
        vm.newTodo.descr = "";
        vm.newTodo.expired_at = null;
        vm.newTodo.prio = 0;
        _.forEach(vm.day.todo_set, function(todo){
            todo.edit=false;
        });
    }

  }
})();