(function () {
  'use strict';

  angular
    .module('jobmanager.todos', [
      'jobmanager.todos.controllers',
      'jobmanager.todos.services'
    ]);

  angular
    .module('jobmanager.todos.controllers', []);

  angular
    .module('jobmanager.todos.services', ['ngCookies']);
})();