/**
* Todos
* @namespace jobmanager.todos.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.todos.services')
    .factory('Todos', Todos);

  Todos.$inject = ['$http'];

function Todos($http) {
    var Todos = {
        all: all,
        create: create,
        get: get,
        destroy: destroy,
        update: update
    };

    return Todos;

    function all() {
        return $http.get('/api/v1/todos/');
    }

    function create(todo) {
        if (!todo.employer){todo.employer={}}
        if (!todo.project){todo.project={}}
        if (!todo.task){todo.task={}}
        if (!todo.client){todo.client={}}
        return $http.post('/en/api/v1/todos/', {
            employer: todo.employer.id,
            project: todo.project.id,
            task: todo.task.id,
            client: todo.client.id,
            descr: todo.descr,
            expired_at: todo.expired_at,
            prio: todo.prio
        });
    }

    function get(username) {
        return $http.get('/en/api/v1/accounts/' + username + '/todos/');
    }

    function destroy(todo){
        return $http.delete('/en/api/v1/todos/' + todo.id + '/');
    }

    function update(todo) {
        var todoToUpdate = _.clone(todo);

        if(todo.employer && typeof todo.employer === 'object'){
            todoToUpdate.employer = todo.employer.id
        }
        if(todo.project && typeof todo.project === 'object'){
            todoToUpdate.project = todo.project.id
        }
        if(todo.task && typeof todo.task === 'object'){
            todoToUpdate.task = todo.task.id
        }
        if(todo.client && typeof todo.client === 'object'){
            todoToUpdate.client = todo.client.id
        }
        return $http.put('/en/api/v1/todos/' + todo.id + '/', todoToUpdate);
    }
}
})();