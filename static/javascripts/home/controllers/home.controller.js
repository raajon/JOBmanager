
(function () {
    'use strict';

    angular
        .module('jobmanager.home.controllers')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$location', '$scope', '$route', 'Authentication'];

    function HomeController($location, $scope, $route, Authentication) {
        var vm = this;

        var options = [
          {selector: '#track', offset: 200, callback: function(el) {
            Materialize.showStaggeredList($(el));
          } },
          {selector: '#todos', offset: 200, callback: function(el) {
            Materialize.showStaggeredList($(el));
          } },
          {selector: '#reports', offset: 200, callback: function(el) {
            Materialize.showStaggeredList($(el));
          } },
          {selector: '#responsive', offset: 200, callback: function(el) {
            Materialize.showStaggeredList($(el));
          } },
          {selector: '#logo', offset: 100, callback: function(el) {
            Materialize.fadeInImage($(el));
          } }
        ];
        Materialize.scrollFire(options);
    }
})();