     (function () {
  'use strict';

  angular
    .module('jobmanager.home', [
      'jobmanager.home.controllers'
    ]);

  angular
    .module('jobmanager.home.controllers', []);

  angular
    .module('jobmanager.home.services', ['ngCookies']);
})();
