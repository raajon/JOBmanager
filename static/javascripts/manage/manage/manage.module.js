(function () {
  'use strict';

  angular
    .module('jobmanager.manage', [
      'jobmanager.manage.services'
    ]);

  angular
    .module('jobmanager.manage.services', ['ngCookies']);
})();