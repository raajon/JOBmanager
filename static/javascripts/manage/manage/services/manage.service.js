/**
* Manage
* @namespace jobmanager.manage.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.manage.services')
    .factory('Manage', Manage);

  Manage.$inject = ['$http','$q', '$window'];

  function Manage($http, $q, $window) {
    var Manage = {
      get: get
    };

    return Manage;

//    function get(username) {
//      return $http.get('/en/api/v1/manage/' + username + '/');
//    }

    function get(username) {
        var deferred = $q.defer();
        var localManage = angular.fromJson($window.localStorage.getItem("manage"));
        var strLocalManage = ""
        if(localManage){
            strLocalManage = localManage.version
        }else{
            strLocalManage = 9999999999
        }
        $http.get('/en/api/v1/accounts/' + username + '/manage/' + strLocalManage + '/').then(
        function(data, status, headers, config) {
            if(!data.data[0]){
                deferred.resolve(localManage);
            }else{
                $window.localStorage.setItem("manage",angular.toJson(data.data[0]));
                deferred.resolve(data.data[0]);
            }
        },function(data, status, headers, config) {
            deferred.reject(data);
        });
        return deferred.promise;
    }
  }
})();