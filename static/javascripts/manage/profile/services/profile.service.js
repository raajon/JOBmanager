/**
* Profile
* @namespace jobmanager.profile.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.profile.services')
    .factory('Profile', Profile);

  Profile.$inject = ['$http'];

  function Profile($http) {
    var Profile = {
      get: get,
      update: update
    };

    return Profile;

    function get(username) {
      return $http.get('/en/api/v1/accounts/' + username + '/profile/');
    }

    function update(profile) {
        return $http.put('/en/api/v1/profile/' + profile.id + '/', profile);
    }
  }
})();