/**
* LoginController
* @namespace jobmanager.profile.controllers
*/
(function () {
  'use strict';

angular
.module('jobmanager.profile.controllers')
.controller('ProfileController', ProfileController);

ProfileController.$inject = ['$location', '$scope', 'Authentication', '$filter', 'Profile'];


function ProfileController($location, $scope, Authentication, $filter, Profile) {
    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.editProfile = {};
    vm.profile = {};

    vm.editProfileFn = function(){
        vm.editProfile.id = vm.profile.id;
        vm.editProfile.username = vm.profile.username;
        vm.editProfile.first_name = vm.profile.first_name;
        vm.editProfile.last_name = vm.profile.last_name;
        vm.editProfile.email = vm.profile.email;
    }

    vm.updateProfile = function(profile) {
        vm.edit=false;
        Authentication.update(profile).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.profileupdated.success"),2000);
                vm.getProfile();
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.profileupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.getProfile = function(){
        Authentication.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.profile = data.data;
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    activate();
    function activate() {
        vm.getProfile();
    }
}
})();