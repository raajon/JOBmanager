(function () {
  'use strict';

  angular
    .module('jobmanager.profile', [
      'jobmanager.profile.controllers',
      'jobmanager.profile.services'
    ]);

  angular
    .module('jobmanager.profile.controllers', []);

  angular
    .module('jobmanager.profile.services', ['ngCookies']);
})();