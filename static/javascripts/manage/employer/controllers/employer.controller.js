/**
* LoginController
* @namespace jobmanager.employers.controllers
*/
(function () {
  'use strict';

angular
.module('jobmanager.employers.controllers')
.controller('EmployersController', EmployersController);

EmployersController.$inject = ['$location', '$scope', 'Authentication', '$filter', 'Employers', 'Manage'];


function EmployersController($location, $scope, Authentication, $filter, Employers, Manage) {
    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();


    vm.add=false;
    vm.edit=false;
    vm.newEmployer = {
        name:"",
        project:[],
        descr:""
    };
    vm.editEmployer = {};
    vm.employers= []


    vm.editEmployerFn = function(employer){
        vm.editEmployer.id = employer.id;
        vm.editEmployer.name = employer.name;
        vm.editEmployer.default = employer.default;

        vm.add=false;
        vm.edit=true;
    }
    vm.newEmployerFn = function(client){
        vm.editEmployer.id = null;
        vm.editEmployer.name = "";
        vm.editEmployer.default = false;

        vm.add=true;
        vm.edit=false;
    }
    vm.saveEmployer = function(newEmployer) {
        vm.employers.push(newEmployer);
        vm.add=false;
        if (newEmployer.default){
            var employer = _.findWhere(vm.employers, {'default': true});
            employer.default = false;
            vm.updateEmployer(employer);
        }
        Employers.create(newEmployer).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.employercreated.success"),2000);
                vm.getManage();
            },
            function(data, status, headers, config) {
                vm.employers.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.employercreated.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateEmployer = function(newEmployer) {
        vm.edit=false;
        if (newEmployer.default){
            var employer = _.findWhere(vm.employers, {'default': true});
            if(employer){
                employer.default = false;
                vm.updateEmployer(employer);
            }
        }
        Employers.update(newEmployer).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.employerupdated.success"),2000);
                vm.getManage();
            },
            function(data, status, headers, config) {
                vm.employers.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.employerupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.getManage = function(){
        Manage.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.employers = data.employer_set;
                vm.employer=vm.employers[0];
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }
    vm.doneEmployer = function(employer){
        employer.done=true;
        Employers.update(employer).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.employerupdated.success"),2000);
                vm.employers =_.without(vm.employers, employer);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.employerupdated.error"),2000, 'error-toast');
            }
        );
    }

    activate();
    function activate() {
        vm.getManage();
    }
}
})();