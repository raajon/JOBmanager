/**
* Employers
* @namespace jobmanager.employers.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.employers.services')
    .factory('Employers', Employers);

  Employers.$inject = ['$http'];

  function Employers($http) {
    var Employers = {
      all: all,
      create: create,
      get: get,
      update: update
    };

    return Employers;

    function all() {
      return $http.get('/en/api/v1/employers/');
    }

    function create(employer) {
      return $http.post('/en/api/v1/employers/', {
        name: employer.name,
        project_set:{},
        default: employer.default
      });
    }

    function get(username) {
      return $http.get('/en/api/v1/accounts/' + username + '/employers/');
    }

    function update(employer) {
        if(!employer.project_set){
            employer.project_set={}
        }
        return $http.put('/en/api/v1/employers/' + employer.id + '/', employer);
    }
  }
})();