(function () {
  'use strict';

  angular
    .module('jobmanager.employers', [
      'jobmanager.employers.controllers',
      'jobmanager.employers.services'
    ]);

  angular
    .module('jobmanager.employers.controllers', []);

  angular
    .module('jobmanager.employers.services', ['ngCookies']);
})();