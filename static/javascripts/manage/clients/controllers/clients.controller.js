/**
* LoginController
* @namespace jobmanager.clients.controllers
*/
(function () {
  'use strict';

angular
.module('jobmanager.clients.controllers')
.controller('ClientsController', ClientsController);

ClientsController.$inject = ['$location', '$scope', 'Authentication', '$filter', 'Clients', 'Projects', 'Manage'];


function ClientsController($location, $scope, Authentication, $filter, Clients, Projects, Manage) {
    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();


    vm.add=false;
    vm.edit=false;
    vm.addable =true
    vm.newClient = {
        name:"",
        project:[],
        phone_number:"",
        email:"",
        address:"",
        descryption:""
    };
    vm.editClient = {};
    vm.clients= [];
    vm.projects= [];


    vm.editClientFn = function(client){
        vm.editClient.id = client.id;
        vm.editClient.name = client.name;
        vm.editClient.project = client.project;
        vm.editClient.phone_number = client.phone_number;
        vm.editClient.email = client.email;
        vm.editClient.address = client.address;
        vm.editClient.descryption = client.descryption;

        vm.add=false;
        vm.edit=true;
    }
    vm.newClientFn = function(client){
        vm.editClient.id = null;
        vm.editClient.name = "";
        vm.editClient.project = "";
        vm.editClient.phone_number = "";
        vm.editClient.email = "";
        vm.editClient.address = "";
        vm.editClient.descryption = "";

        vm.add=true;
        vm.edit=false;
    }

    vm.saveClient = function(newClient) {
        vm.clients.push(newClient);
        vm.add=false;
        Clients.create(newClient).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.clientcreated.success"),2000);
                vm.getManage();
            },
            function(data, status, headers, config) {
                vm.clients.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.clientcreated.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateClient = function(client) {
        vm.edit=false;
        Clients.update(client).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.clientupdated.success"),2000);
                vm.getManage();
            },
            function(data, status, headers, config) {
                vm.clients.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.clientupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.getManage = function(){
        Manage.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.clients = data.client_set;
                vm.client=vm.clients[0];
                vm.projects = data.project_set;
                vm.project=vm.projects[0];
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    vm.isActive = function(array, id){
        return _.indexOf(array, id)>-1;
    }
    vm.addClientToProject = function(project, client){
        client.project.push(project.id);
        Clients.update(client).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.clientupdated.success"),2000);
            },
            function(data, status, headers, config) {
                var index = client.project.indexOf(project.id);
                if (index > -1) {
                    client.project.splice(index, 1);
                }
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.clientupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.removeClientToProject = function(project, client){
        var index = client.project.indexOf(project.id);
        if (index > -1) {
            client.project.splice(index, 1);
        }
        Clients.update(client).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.clientupdated.success"),2000);
            },
            function(data, status, headers, config) {
                client.project.push(project.id);
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.clientupdated.error"),2000, 'error-toast');
            }
        );
    }

    activate();
    function activate() {
        vm.getManage();
    }
}
})();