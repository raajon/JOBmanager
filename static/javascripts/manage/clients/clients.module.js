(function () {
  'use strict';

  angular
    .module('jobmanager.clients', [
      'jobmanager.clients.controllers',
      'jobmanager.clients.services'
    ]);

  angular
    .module('jobmanager.clients.controllers', []);

  angular
    .module('jobmanager.clients.services', ['ngCookies']);
})();