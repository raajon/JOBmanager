/**
* Clients
* @namespace jobmanager.clients.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.clients.services')
    .factory('Clients', Clients);

  Clients.$inject = ['$http'];

  function Clients($http) {
    var Clients = {
      all: all,
      create: create,
      get: get,
      update: update
    };

    return Clients;

    function all() {
      return $http.get('/en/api/v1/clients/');
    }

    function create(client) {
      return $http.post('/en/api/v1/clients/', {
        name: client.name,
        project: client.project,
        phone_number: client.phone_number,
        email: client.email,
        address: client.address,
        descryption: client.descryption
      });
    }

    function get(username) {
      return $http.get('/en/api/v1/accounts/' + username + '/clients/');
    }

    function update(client) {
      return $http.put('/en/api/v1/clients/' + client.id + '/', client);
    }
  }
})();