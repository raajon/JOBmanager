/**
* LoginController
* @namespace jobmanager.tasks.controllers
*/
(function () {
  'use strict';

angular
.module('jobmanager.tasks.controllers')
.controller('TasksController', TasksController);

TasksController.$inject = ['$location', '$scope', 'Authentication', '$filter', 'Tasks', 'Projects', 'Manage'];


function TasksController($location, $scope, Authentication, $filter, Tasks, Projects, Manage) {
    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();


    vm.add=false;
    vm.edit=false;
    vm.newTask = {
        name:"",
        project:[],
        descr:""
    };
    vm.editTask = {};
    vm.tasks= [];
    vm.projects= [];

    vm.editTaskFn = function(task){
        vm.editTask.id = task.id;
        vm.editTask.name = task.name;
        vm.editTask.project = task.project;
        vm.editTask.descr = task.descr;

        vm.add=false;
        vm.edit=true;
    }
    vm.newTaskFn = function(client){
        vm.editTask.id = null;
        vm.editTask.name = "";
        vm.editTask.project = "";
        vm.editTask.descr = "";

        vm.add=true;
        vm.edit=false;
    }
    vm.saveTask = function(newTask) {
        vm.tasks.push(newTask);
        vm.add=false;
        Tasks.create(newTask).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.taskcreated.success"),2000);
                vm.getTasks();
            },
            function(data, status, headers, config) {
                vm.tasks.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.taskcreated.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateTask = function(task) {
        vm.edit=false;
        Tasks.update(task).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.taskupdated.success"),2000);
                vm.getTasks();
            },
            function(data, status, headers, config) {
                vm.tasks.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.taskupdated.error"),2000, 'error-toast');
            }
        );
    }

    vm.isActive = function(array, id){
        return _.indexOf(array, id)>-1;
    }
    vm.addTaskToProject = function(project, task){
        task.project.push(project.id);
        Tasks.update(task).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.taskupdated.success"),2000);
            },
            function(data, status, headers, config) {
                var index = task.project.indexOf(project.id);
                if (index > -1) {
                    task.project.splice(index, 1);
                }
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.taskupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.removeTaskToProject = function(project, task){
        var index = task.project.indexOf(project.id);
        if (index > -1) {
            task.project.splice(index, 1);
        }
        Tasks.update(task).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.taskupdated.success"),2000);
            },
            function(data, status, headers, config) {
                task.project.push(project.id);
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.taskupdated.error"),2000, 'error-toast');
            }
        );
    }

    vm.getManage = function(){
        Manage.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.tasks = data.task_set;
                vm.task=vm.tasks[0];
                vm.projects = data.project_set;
                vm.project=vm.projects[0];
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    activate();
    function activate() {
        vm.getManage();
    }
}
})();