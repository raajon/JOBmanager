/**
* Tasks
* @namespace jobmanager.tasks.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.tasks.services')
    .factory('Tasks', Tasks);

  Tasks.$inject = ['$http'];

  function Tasks($http) {
    var Tasks = {
      all: all,
      create: create,
      get: get,
      update: update
    };

    return Tasks;

    function all() {
      return $http.get('/en/api/v1/tasks/');
    }

    function create(task) {
      return $http.post('/en/api/v1/tasks/', {
        name: task.name,
        project: task.project,
        descr: task.descr
      });
    }

    function get(username) {
      return $http.get('/en/api/v1/accounts/' + username + '/tasks/');
    }

    function update(task) {
      return $http.put('/en/api/v1/tasks/' + task.id + '/', task);
    }
  }
})();