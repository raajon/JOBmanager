(function () {
  'use strict';

  angular
    .module('jobmanager.tasks', [
      'jobmanager.tasks.controllers',
      'jobmanager.tasks.services'
    ]);

  angular
    .module('jobmanager.tasks.controllers', []);

  angular
    .module('jobmanager.tasks.services', ['ngCookies']);
})();