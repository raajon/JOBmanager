(function () {
  'use strict';

  angular
    .module('jobmanager.projects', [
      'jobmanager.projects.controllers',
      'jobmanager.projects.services'
    ]);

  angular
    .module('jobmanager.projects.controllers', []);

  angular
    .module('jobmanager.projects.services', ['ngCookies']);
})();