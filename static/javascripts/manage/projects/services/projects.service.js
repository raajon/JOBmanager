/**
* Projects
* @namespace jobmanager.projects.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.projects.services')
    .factory('Projects', Projects);

  Projects.$inject = ['$http', '$q', '$window'];

  function Projects($http, $q, $window) {
    var Projects = {
      all: all,
      create: create,
      get: get,
      update:update,
      getLog:getLog,
    };

    return Projects;

    function all() {
      return $http.get('/en/api/v1/projects/');
    }

    function create(project) {
      return $http.post('/en/api/v1/projects/', {
        name: project.name,
        employer: project.employer.id?project.employer.id:project.employer,
        descr: project.descr
      });
    }

    function get(username) {
      return $http.get('/en/api/v1/accounts/' + username + '/projects/');
    }

    function update(project) {
        var projectToUpdate = _.clone(project);
        if(project.employer && typeof project.employer === 'object'){
            projectToUpdate.employer = project.employer.id
        }
        return $http.put('/en/api/v1/projects/' + project.id + '/', projectToUpdate);
    }

    function getLog(username, project) {
        return $http.get('/en/api/v1/accounts/' + username + '/logproject/' + project + '/');
    }

    function getLog(username, project) {
        var deferred = $q.defer();
        var localProject = angular.fromJson($window.localStorage.getItem('project-' + project));
        var strLocalProject = ""
        if(localProject){
            strLocalProject = project + "-" + localProject.version
        }else{
            strLocalProject = project
        }
        $http.get('/en/api/v1/accounts/' + username + '/logproject/' + strLocalProject + '/').then(
        function(data, status, headers, config) {
            if(!data.data[0]){
                deferred.resolve(localProject);
            }else{
                $window.localStorage.setItem('project-' + project,angular.toJson(data.data[0]));
                deferred.resolve(data.data[0]);
            }
        },function(data, status, headers, config) {
            deferred.reject(data);
        });
        return deferred.promise;
    }
  }
})();