/**
* LoginController
* @namespace jobmanager.projects.controllers
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.projects.controllers')
    .controller('ProjectsController', ProjectsController);

  ProjectsController.$inject = ['$location', '$scope', 'Authentication', '$filter', 'Employers', 'Projects', 'Tasks', 'Clients', 'Manage'];

  /**
  * @namespace ProjectsController
  */
function ProjectsController($location, $scope, Authentication, $filter, Employers, Projects, Tasks, Clients, Manage) {
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();

    vm.submenu='current';
    vm.add=false;
    vm.edit=false;
    vm.newProject = {
        name:"",
        employer:{},
        descr:""
    };
    vm.employers= [];
    vm.projects= [];
    vm.tasks= [];
    vm.clients= [];
    vm.editProject={
        name:"",
        employer:{},
        descr:""
    };
    vm.project={};

    vm.getEmployerName = function(id){
        var employer = _.findWhere(vm.employers, {id : id} )
        if(employer){
            return employer.name;
        }
    };

    vm.editProjectFn = function(project){
        vm.edit=true;
        vm.editProject.id = project.id;
        vm.editProject.name = project.name;
        vm.editProject.employer = project.employer;
        vm.editProject.descr = project.descr;

        vm.add=false;
        vm.edit=true;
    }
    vm.newProjectFn = function(project){
        vm.editProject.id = null;
        vm.editProject.name = "";
        vm.editProject.employer = _.findWhere(vm.employers, {'default': true});
        vm.editProject.descr = "";

        vm.add=true;
        vm.edit=false;
    }
    vm.saveProject = function(newProject) {
        vm.projects.push(newProject);
        vm.add=false;
        Projects.create(newProject).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectcreated.success"),2000);
                vm.getManage();
            },
            function(data, status, headers, config) {
                vm.projects.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectcreated.error"),2000, 'error-toast');
            }
        );
    }
    vm.updateProject = function(project) {
        vm.edit=false;
        Projects.update(project).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectupdated.success"),2000);
                vm.getManage();
            },
            function(data, status, headers, config) {
                vm.projects.pop();
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectupdated.error"),2000, 'error-toast');
            }
        );
    }
    vm.addTaskToProject = function(project, task){
        task.project.push(project.id);
        Tasks.update(task).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectupdated.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectupdated.error"),2000, 'error-toast');
                vm.getManage();
            }
        );
    }
    vm.removeTaskToProject = function(project, task){
        var index = task.project.indexOf(project.id);
        if (index > -1) {
            task.project.splice(index, 1);
        }
        Tasks.update(task).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectupdated.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectupdated.error"),2000, 'error-toast');
                vm.getManage();
            }
        );
    }
    vm.addClientToProject = function(project, client){
        client.project.push(project.id);
        Clients.update(client).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectupdated.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectupdated.error"),2000, 'error-toast');
                vm.getManage();
            }
        );
    }
    vm.removeClientToProject = function(project, client){
        var index = client.project.indexOf(project.id);
        if (index > -1) {
            client.project.splice(index, 1);
        }
        Clients.update(client).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectupdated.success"),2000);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectupdated.error"),2000, 'error-toast');
                vm.getManage();
            }
        );
    }
    vm.doneProject = function(project, state){
        project.done=state;
        Projects.update(project).then(
            function(data, status, headers, config) {
                Materialize.toast($filter('i18n')("msg.projectupdated.success"),2000);
//                vm.projects =_.without(vm.projects, project);
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18n')("msg.projectupdated.error"),2000, 'error-toast');
            }
        );
    }

    vm.isActive = function(array, id){
        return _.indexOf(array, id)>-1;
    }

    vm.getManage = function(){
        Manage.get(Authentication.getAuthenticatedAccount().username).then(
            function(data, status, headers, config) {
                vm.employers = data.employer_set;
                vm.tasks = data.task_set;
                vm.clients = data.client_set;
                vm.projects = data.project_set;
                vm.project=vm.projects[0];
                _.forEach(vm.projects, function(project){
                    project.employer = _.findWhere(vm.employers, {id : project.employer} )
                });
            },
            function(data, status, headers, config) {
                console.log(data.error);
            }
        );
    }

    activate();
    function activate() {
        vm.getManage();
    }


  }
})();