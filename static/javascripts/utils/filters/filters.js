angular.module('filters', [])
.filter('tasksByProject', function() {
    return function(tasks, project) {
        if(project){
            var tasks = _.filter(tasks, function(task){
                var i = _.indexOf(task.project, project.id);
                if(i>=0){
                    return true;
                }else{
                    return false;
                }
            });
            return tasks
        }
        return [];
    };
})
.filter('byEmployer', function() {
    return function(stuffs, employer) {
        if(employer){
            var stuffs = _.filter(stuffs, function(stuff){
                if(stuff.employer){
                    if (stuff.employer == employer.id){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            });
            return stuffs
        }
        return [];
    };
})
.filter('i18n', function() {
    return function(x) {
        var out = gettext(x);
        if(!out){
            out=x;
        }
        return out;
    };
})
.filter("trustUrl", ['$sce', function ($sce) {
    return function (recordingUrl) {
        return $sce.trustAsResourceUrl(recordingUrl);
    };
}])
.filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);