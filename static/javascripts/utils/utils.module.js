(function () {
  'use strict';

  angular
    .module('jobmanager.utils', [
      'jobmanager.utils.services'
    ]);

  angular
    .module('jobmanager.utils.services', []);
})();