/**
* Day
* @namespace jobmanager.day.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.day.services')
    .factory('Day', Day);

  Day.$inject = ['$http'];

function Day($http) {
    var Day = {
        all: all,
        create: create,
        get: get,
        destroy: destroy,
        update: update
    };

    return Day;

    function all() {
        return $http.get('/api/v1/day/');
    }

    function create(day) {
        day.date = moment(day.date).format('YYYY-MM-DD');
        return $http.post('/en/api/v1/day/', day);
    }

    function get(username, day) {
        return $http.get('/en/api/v1/accounts/' + username + '/day/' + day + '/');
    }

    function destroy(day){
        return $http.delete('/en/api/v1/day/' + day.id + '/');
    }

    function update(day) {
      return $http.put('/en/api/v1/day/' + day.id + '/', day);
    }
}
})();