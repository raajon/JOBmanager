(function () {
  'use strict';

  angular
    .module('jobmanager.day', [
      'jobmanager.day.controllers',
      'jobmanager.day.services'
    ]);

  angular
    .module('jobmanager.day.controllers', []);

  angular
    .module('jobmanager.day.services', ['ngCookies']);
})();