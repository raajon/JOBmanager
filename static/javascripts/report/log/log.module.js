(function () {
  'use strict';

  angular
    .module('jobmanager.log', [
      'jobmanager.log.controllers'
    ]);

  angular
    .module('jobmanager.log.controllers', []);

})();