
(function () {
  'use strict';

  angular
    .module('jobmanager.log.controllers')
    .controller('LogController', LogController);

  LogController.$inject = ['$location', '$route', '$scope', '$filter', '$locale', 'Authentication', 'Month'];

  function LogController($location, $route, $scope, $filter, $locale, Authentication, Month) {
    var vm = this;
    vm.locale = $locale.DATETIME_FORMATS;
    vm.daylog = false;
    vm.tracklog = false;
    vm.statslog = false;

    vm.isAuthenticated = Authentication.isAuthenticated();

    vm.path=$route.current.params
    if(vm.path.kind=="days"){
        vm.daylog = true;
    }else if(vm.path.kind=="tracks"){
        vm.tracklog = true;
    }else if(vm.path.kind=="stats"){
        vm.statslog = true;
    }else{
        vm.statslog = true;
    }

    vm.month= {
        tracks:[]
    }
    if(vm.path.dateFrom && vm.path.dateTo){
        vm.monthFrom = new Date(vm.path.dateFrom);
        vm.monthTo = new Date(vm.path.dateTo);
    }else{
        vm.monthFrom = new Date();
        vm.monthTo = new Date();

        vm.monthFrom.setDate(1);
        vm.monthTo.setMonth(vm.monthFrom.getMonth()+1);
        vm.monthTo.setDate(vm.monthFrom.getDate()-1);
    }

    vm.monthFrom.setUTCHours(0);
    vm.monthFrom.setUTCMinutes(0);
    vm.monthFrom.setUTCSeconds(0);
    vm.monthFrom.setUTCMilliseconds(0);

    vm.monthTo.setUTCHours(0);
    vm.monthTo.setUTCMinutes(0);
    vm.monthTo.setUTCSeconds(0);
    vm.monthTo.setUTCMilliseconds(0);

    vm.stats = {
            employers: [],
            doughnutEmployerData: [],
            doughnutEmployerLabel: [],
            projects: [],
            doughnutProjectData: [],
            doughnutProjectLabel: [],
            tasks: [],
            doughnutTaskData: [],
            doughnutTaskLabel: [],
            clients: [],
            doughnutClientData: [],
            doughnutClientLabel: [],
            total:0,
        selections :{
            employers: [],
            projects: [],
            tasks: [],
            clients: []
        },
        toSelect :{
            employers: [],
            projects: [],
            tasks: [],
            clients: []
        }
    };
    vm.doughnutOptions={
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
            label: function(tooltipItem, data) {
                var label = data.labels[tooltipItem.index];
                var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return label + ': ' + $filter('duration')(datasetLabel, "hh:mm");
                }
            }
        }
    }

    vm.getDiff = function(date1, date2){
        if (!date1){date1=Date.now();}
        var diff = (new Date(date1).getTime()- new Date(date2).getTime());
        return new Date(diff);
    };

    vm.isDateToShow = function(i){
        var value= true;
        if(vm.month.track_set[i-1]){
            if(new Date(vm.month.track_set[i].started_at).getDate() == new Date(vm.month.track_set[i-1].started_at).getDate()){

                if(vm.month.track_set[i-1].show){
                    value=false;
                }
            }
        }
        return value;
    }

    function activate() {
        vm.getData();
    }

    vm.getData = function(){
        vm.month = {
            track_set:[],
            day_set:[]
        }
        vm.monthFrom = new Date(vm.monthFrom);
        vm.monthTo = new Date(vm.monthTo);
        $location.url('/' + vm.path.language + '/log/' + vm.path.kind + '/' +  moment(vm.monthFrom).format('YYYY-MM-DD') + '/' +  moment(vm.monthTo).format('YYYY-MM-DD'));
        vm.getDataMonth(new Date(vm.monthFrom));
    }

    vm.getDataMonth = function(from){
        var formattedDate = moment(from).format('YYYY-MM');
        Month.get(Authentication.getAuthenticatedAccount().username, formattedDate).then(
            function(data, status, headers, config) {
//                var data = data.data[0];
                if(data){
                    _.forEach(data.track_set, function(track){
                        if(new Date(track.started_at)>=vm.monthFrom && new Date(track.started_at)<=vm.monthTo){
                            vm.month.track_set.push(track);
                        }
                    });
                    _.forEach(data.day_set, function(day){
                        if(new Date(day.date)>=vm.monthFrom && new Date(day.date)<=vm.monthTo){
                            vm.month.day_set.push(day);
                        }
                    });
                }
                var nextMonth = new Date(from).setMonth(from.getMonth()+1);
                if(nextMonth<=vm.monthTo){
                    from.setDate(1);
                    from.setMonth(from.getMonth()+1);
                    vm.getDataMonth(from);
                }else{
                    for (var d = new Date(vm.monthFrom); d <= new Date(vm.monthTo); d.setDate(d.getDate() + 1)) {
                        var day = _.filter(vm.month.day_set, function(day){
                            var y = new Date(day.date).getYear();
                            var m = new Date(day.date).getMonth();
                            var dd = new Date(day.date).getDate();
                            return d.getYear()==y && d.getMonth()==m && d.getDate()==dd;
                        });
                        if(!day[0]){
                            vm.month.day_set.push({date:new Date(d)});
                        }
                    }
                    vm.month.day_set = _.sortBy(vm.month.day_set, 'date');
                    vm.month.track_set = _.sortBy(vm.month.track_set, 'started_at');
                    vm.initSelections();
                    vm.updateStats();
                }
            },
            function(data, status, headers, config) {
                console.log(data.error);
                Materialize.toast($filter('i18')("msg.log.error"),2000, 'error-toast');
            }
        );
    }

    vm.updateStats = function(){
        vm.stats.employers = [];
        vm.stats.projects = [];
        vm.stats.tasks = [];
        vm.stats.clients = [];
        vm.stats.total = 0;

        _.forEach(vm.month.track_set, function(track){
            var trackDuration = 0;
            if (track.stoped_at){
                trackDuration= (new Date(track.stoped_at).getTime()-new Date(track.started_at).getTime());
            }
            track.show=false;

            if( vm.stats.selections.employers.indexOf(track.employer? track.employer.name: $filter('i18n')("field.other"))>=0 &&
                vm.stats.selections.projects.indexOf(track.project? track.project.name: $filter('i18n')("field.other"))>=0 &&
                vm.stats.selections.tasks.indexOf(track.task? track.task.name: $filter('i18n')("field.other"))>=0 &&
                vm.stats.selections.clients.indexOf(track.client? track.client.name: $filter('i18n')("field.other"))>=0 ){

                vm.collectEmployers(track, trackDuration);
                vm.collectProjects(track, trackDuration);
                vm.collectTasks(track, trackDuration);
                vm.collectClients(track, trackDuration);

                track.show=true;
                vm.stats.total = vm.stats.total + trackDuration;
            }
        });
        vm.clalculateDays();
    }

    vm.initSelections = function(){
        _.forEach(vm.month.track_set, function(track){

            var trackDuration = 0;
            if (track.stoped_at){
                trackDuration= (new Date(track.stoped_at).getTime()-new Date(track.started_at).getTime());
            }

            vm.collectEmployers(track, trackDuration);
            vm.collectProjects(track, trackDuration);
            vm.collectTasks(track, trackDuration);
            vm.collectClients(track, trackDuration)

            track.show=true;
            vm.stats.total = vm.stats.total + trackDuration;
        });
        vm.stats.employers=_.sortBy(vm.stats.employers, function(employer){ return -employer.duration; });
        vm.stats.projects=_.sortBy(vm.stats.projects, function(project){ return -project.duration; });
        vm.stats.tasks=_.sortBy(vm.stats.tasks, function(task){ return -task.duration; });
        vm.stats.clients=_.sortBy(vm.stats.clients, function(client){ return -client.duration; });

        _.forEach(vm.stats.employers, function(employer){
            if(employer.default || vm.stats.employers.length==1){
                vm.stats.selections.employers.push(employer.name);
            }
            vm.stats.toSelect.employers.push(employer);
            vm.stats.doughnutEmployerData.push(employer.duration);
            vm.stats.doughnutEmployerLabel.push(employer.name);
        });
        _.forEach(vm.stats.projects, function(project){
            vm.stats.selections.projects.push(project.name);
            vm.stats.toSelect.projects.push(project);
            vm.stats.doughnutProjectData.push(project.duration);
            vm.stats.doughnutProjectLabel.push(project.name);
        });
        _.forEach(vm.stats.tasks, function(task){
            vm.stats.selections.tasks.push(task.name);
            vm.stats.toSelect.tasks.push(task);
            vm.stats.doughnutTaskData.push(task.duration);
            vm.stats.doughnutTaskLabel.push(task.name);
        });
        _.forEach(vm.stats.clients, function(client){
            vm.stats.selections.clients.push(client.name);
            vm.stats.toSelect.clients.push(client);
            vm.stats.doughnutClientData.push(client.duration);
            vm.stats.doughnutClientLabel.push(client.name);
        });
        vm.clalculateDays();
    }

    vm.collectEmployers = function(track, trackDuration){
        var employer = _.findWhere(vm.stats.employers, {name: track.employer? track.employer.name: $filter('i18n')("field.other")})
        if(employer){
            employer.duration = employer.duration + trackDuration;
        }else{
            var employer = {
                name:track.employer? track.employer.name: $filter('i18n')("field.other"),
                duration: trackDuration,
                default:track.employer? track.employer.default:false
            }
            vm.stats.employers.push(employer);
        }
    }

    vm.collectProjects = function(track, trackDuration){
        var project = _.findWhere(vm.stats.projects, {name: track.project? track.project.name: $filter('i18n')("field.other")})
        if(project){
            var task2 = _.findWhere(project.tasks, {name: track.task? track.task.name: $filter('i18n')("field.other")});
            project.duration = project.duration + trackDuration;
            if(task2){
                task2.duration = task2.duration + trackDuration;
            }else{
                project.tasks.push({
                    name: track.task? track.task.name: $filter('i18n')("field.other"),
                    duration: trackDuration
                });
            }
        }else{
            var project = {
                name:track.project? track.project.name: $filter('i18n')("field.other"),
                duration: trackDuration,
                tasks: [{
                    name: track.task? track.task.name: $filter('i18n')("field.other"),
                    duration: trackDuration
                }]
            }
            vm.stats.projects.push(project);
        }
    }

    vm.collectTasks = function(track, trackDuration){
        var task = _.findWhere(vm.stats.tasks, {name: track.task? track.task.name: $filter('i18n')("field.other")});
        if(task){
            task.duration = task.duration + trackDuration;
        }else{
            vm.stats.tasks.push({
                name:track.task? track.task.name: $filter('i18n')("field.other"),
                duration: trackDuration
            });
        }
    }

    vm.collectClients = function(track, trackDuration){
        var client = _.findWhere(vm.stats.clients, {name: track.client? track.client.name: $filter('i18n')("field.other")})
        if(client){
            client.duration = client.duration + trackDuration;
        }else{
            var client = {
                name:track.client? track.client.name: $filter('i18n')("field.other"),
                duration: trackDuration
            }
            vm.stats.clients.push(client);
        }
    }

    vm.clalculateDays = function(){
        _.forEach(vm.month.day_set, function(day){
            day.date= new Date(day.date);
            day.duration = 0;
            _.forEach(day.track_set, function(track){
                if( vm.stats.selections.employers.indexOf(track.employer? track.employer.name: $filter('i18n')("field.other"))>=0 &&
                    vm.stats.selections.projects.indexOf(track.project? track.project.name: $filter('i18n')("field.other"))>=0 &&
                    vm.stats.selections.tasks.indexOf(track.task? track.task.name: $filter('i18n')("field.other"))>=0 &&
                    vm.stats.selections.clients.indexOf(track.client? track.client.name: $filter('i18n')("field.other"))>=0 ){
                        if (track.stoped_at){
                            day.duration= day.duration + (new Date(track.stoped_at).getTime()-new Date(track.started_at).getTime());
                        }else{
                            day.duration= day.duration + (Date.now()-new Date(track.started_at).getTime());
                        }
                }
            });
        });
        vm.month.day_set = _.sortBy(vm.month.day_set, 'date');
    }

    activate();

  }
})();