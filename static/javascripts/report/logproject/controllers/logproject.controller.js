
(function () {
  'use strict';

  angular
    .module('jobmanager.logproject.controllers')
    .controller('LogprojectController', LogprojectController);

  LogprojectController.$inject = ['$location', '$route', '$scope', '$filter', '$locale', 'Authentication', 'Manage', 'Projects'];

  function LogprojectController($location, $route, $scope, $filter, $locale, Authentication, Manage, Projects) {
    var vm = this;
    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.path=$route.current.params;
    vm.project={};

    vm.submenu='stats';
    vm.stats = {
            tasks: [],
            doughnutTaskData: [],
            doughnutTaskLabel: [],
            clients: [],
            doughnutClientData: [],
            doughnutClientLabel: [],
            total:0
    }
    vm.doughnutOptions={
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
            label: function(tooltipItem, data) {
                var label = data.labels[tooltipItem.index];
                var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return label + ': ' + $filter('duration')(datasetLabel, "hh:mm");
                }
            }
        }
    }


    Manage.get(Authentication.getAuthenticatedAccount().username).then(
        function(data, status, headers, config) {
            vm.projects = data.project_set;
            vm.projects = _.sortBy(vm.projects, 'name');
            if(vm.path.project){
                vm.project = _.findWhere(vm.projects, {name: vm.path.project});
                if(vm.project && vm.project.id){
                    vm.updateStats(vm.project.id);
                }else{
                    vm.project = {};
                    vm.updateStats(1);
                }

            }
        },
        function(data, status, headers, config) {
            console.log(data.error);
        }
    );

    vm.updateStats = function(id){
        Projects.getLog(Authentication.getAuthenticatedAccount().username, id).then(
            function(data){
                if(data){
                    vm.project = data;
                    if(!vm.project.track_set){
                        vm.project.track_set = [];
                    }
                    vm.project.track_set = _.sortBy(vm.project.track_set, 'started_at');
                    vm.initSelections();
                }else{
                    vm.project.track_set = [];
                }
                $location.url('/' + vm.path.language + '/logproject/' + vm.project.name);
            },function(data){
                console.log(data);
            }
        )
    }

    vm.isDateToShow = function(i){
        var value= true;
        if(vm.project.track_set[i-1]){
            if(new Date(vm.project.track_set[i].started_at).getDate() == new Date(vm.project.track_set[i-1].started_at).getDate()){
                if(vm.project.track_set[i-1].show){
                    value=false;
                }
            }
        }
        return value;
    }

    vm.getDiff = function(date1, date2){
        if (!date1){date1=Date.now();}
        var diff = (new Date(date1).getTime()- new Date(date2).getTime());
        return new Date(diff);
    };

    vm.initSelections = function(){
        _.forEach(vm.project.track_set, function(track){

            var trackDuration = 0;
            if (track.stoped_at){
                trackDuration= (new Date(track.stoped_at).getTime()-new Date(track.started_at).getTime());
            }

            vm.collectTasks(track, trackDuration);
            vm.collectClients(track, trackDuration)

            track.show=true;
            vm.stats.total = vm.stats.total + trackDuration;
        });
        vm.stats.tasks=_.sortBy(vm.stats.tasks, function(task){ return -task.duration; });
        vm.stats.clients=_.sortBy(vm.stats.clients, function(client){ return -client.duration; });


        _.forEach(vm.stats.tasks, function(task){
            vm.stats.doughnutTaskData.push(task.duration);
            vm.stats.doughnutTaskLabel.push(task.name);
        });
        _.forEach(vm.stats.clients, function(client){
            vm.stats.doughnutClientData.push(client.duration);
            vm.stats.doughnutClientLabel.push(client.name);
        });
    }

    vm.collectTasks = function(track, trackDuration){
        var task = _.findWhere(vm.stats.tasks, {name: track.task? track.task.name: $filter('i18n')("field.other")});
        if(task){
            task.duration = task.duration + trackDuration;
        }else{
            vm.stats.tasks.push({
                name:track.task? track.task.name: $filter('i18n')("field.other"),
                duration: trackDuration
            });
        }
    }

    vm.collectClients = function(track, trackDuration){
        var client = _.findWhere(vm.stats.clients, {name: track.client? track.client.name: $filter('i18n')("field.other")})
        if(client){
            client.duration = client.duration + trackDuration;
        }else{
            var client = {
                name:track.client? track.client.name: $filter('i18n')("field.other"),
                duration: trackDuration
            }
            vm.stats.clients.push(client);
        }
    }

  }
})();