(function () {
  'use strict';

  angular
    .module('jobmanager.logproject', [
      'jobmanager.logproject.controllers'
    ]);

  angular
    .module('jobmanager.logproject.controllers', []);

})();