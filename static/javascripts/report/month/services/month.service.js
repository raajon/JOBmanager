/**
* Month
* @namespace jobmanager.month.services
*/
(function () {
  'use strict';

  angular
    .module('jobmanager.month.services')
    .factory('Month', Month);

  Month.$inject = ['$http', '$q', '$window'];

function Month($http, $q, $window) {
    var Month = {
        all: all,
        create: create,
        get: get,
        destroy: destroy,
        update: update
    };

    return Month;

    function all() {
        return $http.get('/api/v1/month/');
    }

    function create(month) {
        month.date = moment(month.date).format('YYYY-MM-DD');
        return $http.post('/en/api/v1/month/', month);
    }

    function get(username, month) {
        var deferred = $q.defer();
        var localMonth = angular.fromJson($window.localStorage.getItem(month));
        var strLocalMonth = ""
        if(localMonth){
            strLocalMonth = month + "-" + localMonth.version
        }else{
            strLocalMonth = month
        }
        $http.get('/en/api/v1/accounts/' + username + '/month/' + strLocalMonth + '/').then(
        function(data, status, headers, config) {
            if(!data.data[0]){
                deferred.resolve(localMonth);
            }else{
                $window.localStorage.setItem(month,angular.toJson(data.data[0]));
                deferred.resolve(data.data[0]);
            }
        },function(data, status, headers, config) {
            deferred.reject(data);
        });
        return deferred.promise;
    }

    function destroy(month){
        return $http.delete('/en/api/v1/month/' + month.id + '/');
    }

    function update(month) {
      return $http.put('/en/api/v1/month/' + month.id + '/', month);
    }
}
})();