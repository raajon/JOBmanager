(function () {
  'use strict';

  angular
    .module('jobmanager.month', [
      'jobmanager.month.controllers',
      'jobmanager.month.services'
    ]);

  angular
    .module('jobmanager.month.controllers', []);

  angular
    .module('jobmanager.month.services', ['ngCookies']);
})();