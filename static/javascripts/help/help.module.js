     (function () {
  'use strict';

  angular
    .module('jobmanager.help', [
      'jobmanager.help.controllers'
    ]);

  angular
    .module('jobmanager.help.controllers', []);

  angular
    .module('jobmanager.help.services', ['ngCookies']);
})();
