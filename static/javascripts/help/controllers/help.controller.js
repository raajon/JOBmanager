
(function () {
    'use strict';

    angular
        .module('jobmanager.help.controllers')
        .controller('HelpController', HelpController);

    HelpController.$inject = ['$location', '$scope', '$route', '$filter', 'Authentication'];

    function HelpController($location, $scope, $route, $filter, Authentication) {
        var vm = this;

        vm.menu=[
            {
                name: "employer",
                i18n: "kind.employers",
                desc: "help.employers.desc",
                video: "employers"
            },
            {
                name: "project",
                i18n: "kind.projects",
                desc: "help.projects.desc",
                video: "projects"
            },
            {
                name: "task",
                i18n: "kind.tasks",
                desc: "help.tasks.desc",
                video: "tasks"
            },
            {
                name: "client",
                i18n: "kind.clients",
                desc: "help.clients.desc",
                video: "clients"
            },
            {
                name: "track",
                i18n: "kind.track",
                desc: "help.tracks.desc",
                video: "tracks"
            },
        ]

        vm.getList = function(desc) {
            var array = [];
            for (var i=0; i<10; i++){
//console.log($filter('i18n')(desc + "." + i) )
//console.log("help.employers.desc.2");
                var trans = $filter('i18n')(desc + "." + i);
                if(trans != desc + "." + i){
                    array.push($filter('i18n')(desc + "." + i))
                }
            }
            return array;
        }


    }
})();
