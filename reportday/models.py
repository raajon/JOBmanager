from django.db import models

from authentication.models import Account
from reportmonth.models import Month


class Day(models.Model):
    author = models.ForeignKey(Account)
    workDegree = models.IntegerField(default=0)
    dayDegree = models.IntegerField(default=0)
    date = models.DateField()
    hollyday = models.BooleanField(default=False)
    freeday = models.BooleanField(default=False)
    month = models.ForeignKey(Month)
    duration = models.IntegerField(default=0)

    def __unicode__(self):
        return '{0}'.format(self.content)