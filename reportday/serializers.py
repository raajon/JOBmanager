from rest_framework import serializers

from reportday.models import Day
from tracks.serializers import TrackSerializerDay


class DaySerializer(serializers.ModelSerializer):
    track_set = TrackSerializerDay(many=True, read_only=True, required=False)

    class Meta:
        model = Day

        fields = ('id', 'workDegree', 'dayDegree', 'date', 'hollyday', 'freeday', 'track_set', 'month', 'duration')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(DaySerializer, self).get_validation_exclusions()

        return exclusions + ['author']