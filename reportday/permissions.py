from rest_framework import permissions


class IsAuthorOfDay(permissions.BasePermission):
    def has_object_permission(self, request, view, day):
        if request.user:
            return day.author == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, day):
        if request.user.is_authenticated():
            return day == request.user.username
        return False