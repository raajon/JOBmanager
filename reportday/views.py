from rest_framework import permissions, viewsets
from rest_framework.response import Response

from reportday.models import Day
from reportday.permissions import IsAuthorOfDay, IsAutorized
from reportday.serializers import DaySerializer
from reportmonth.models import Month


class DayViewSet(viewsets.ModelViewSet):
    queryset = Day.objects.order_by('-created_at')
    serializer_class = DaySerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfDay(),)

    # ta funkcja najprawdopowodniej do calkowitego zaorania
    def perform_create(self, serializer):
        data = self.request.data['date'].split('-')
        month = Month.objects.filter(author=self.request.user, year=data[0], month=data[1])
        if month:
            month = month[0]
        else:
            month = Month(author=self.request.user, year=data[0], month=data[1])
            month.save()
        instance = serializer.save(author=self.request.user, month=month)
        return super(DayViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        month = self.request.data['month']
        oMonth = Month.objects.get(pk=month)
        Month.objects.filter(pk=month).update(version=oMonth.version+1)
        instance = serializer.save()


class AccountDayViewSet(viewsets.ViewSet):
    queryset = Day.objects.select_related('author').all()
    serializer_class = DaySerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)


    def retrieve(self, request, pk=None, account_username=None):
        self.check_object_permissions(request, account_username)
        date=pk.split('-')
        if len(date) == 3:
            dayset = self.queryset.filter(author__username=account_username).filter(date__year=date[0],
                                                                   date__month=date[1],
                                                                   date__day=date[2])
            if not dayset:
                month = Month.objects.filter(author=self.request.user, year=date[0], month=date[1])
                if month:
                    month = month[0]
                else:
                    month = Month(author=self.request.user, year=date[0], month=date[1])
                    month.save()
                day = Day(author=self.request.user, date=pk, month=month)
                day.save()
                dayset = self.queryset.filter(author__username=account_username).filter(date__year=date[0],
                                                                                        date__month=date[1],
                                                                                        date__day=date[2])
        serializer = self.serializer_class(dayset, many=True)
        return Response(serializer.data)