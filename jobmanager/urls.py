from django.conf.urls import url,include
from django.conf.urls.i18n import i18n_patterns

from clients.views import AccountClientsViewSet, ClientViewSet
from employer.views import AccountEmployersViewSet, EmployerViewSet
from reportday.views import DayViewSet, AccountDayViewSet
from reportmonth.views import AccountMonthViewSet, MonthViewSet
from tasks.views import TaskViewSet, AccountTasksViewSet
from jobmanager.views import IndexView

from rest_framework_nested import routers

from authentication.views import AccountViewSet, LoginView, LogoutView, ManageViewSet, RemindView, ChangePassView
from projects.views import AccountProjectsViewSet, ProjectViewSet, AccountLogProjectViewSet
from todos.views import TodoViewSet, AccountTodosViewSet
from tracks.views import TrackViewSet, AccountTracksViewSet

router = routers.SimpleRouter()
router.register(r'accounts', AccountViewSet)
# router.register(r'manage', ManageViewSet)
router.register(r'employers', EmployerViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'tasks', TaskViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'tracks', TrackViewSet)
router.register(r'todos', TodoViewSet)
router.register(r'day', DayViewSet)
router.register(r'month', MonthViewSet)


accounts_router = routers.NestedSimpleRouter(
    router, r'accounts', lookup='account'
)
accounts_router.register(r'employers', AccountEmployersViewSet)
accounts_router.register(r'projects', AccountProjectsViewSet)
accounts_router.register(r'logproject', AccountLogProjectViewSet)
accounts_router.register(r'tasks', AccountTasksViewSet)
accounts_router.register(r'clients', AccountClientsViewSet)
accounts_router.register(r'tracks', AccountTracksViewSet)
accounts_router.register(r'todos', AccountTodosViewSet)
accounts_router.register(r'day', AccountDayViewSet)
accounts_router.register(r'month', AccountMonthViewSet)
accounts_router.register(r'manage', ManageViewSet)

js_info_dict = {
    'packages': ('languages', )
}
urlpatterns = i18n_patterns(
     '',
    url(r'^api/v1/auth/login/$', LoginView.as_view(), name='login'),
    url(r'^api/v1/auth/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^api/v1/auth/remind/$', RemindView.as_view(), name='remind'),
    url(r'^api/v1/auth/changepass/$', ChangePassView.as_view(), name='changepass'),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include(accounts_router.urls)),
    url(r'^api/v1/confirm/(?P<activation_key>\w+)/', ('authentication.views.register_confirm')),
    url(r'^jsi18n/', 'django.views.i18n.javascript_catalog', js_info_dict),
    url('^.*$', IndexView.as_view(), name='index'),
)
