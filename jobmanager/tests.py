from audioop import reverse

from django.core import mail
from django.test import TestCase
from django.utils.translation import activate


class EmailTest(TestCase):
    def test_send_email(self):
        mail.send_mail('Test mail JobOWL', "test", 'contact@jobowl.eu', ['i.niwald@idanet.pl'], fail_silently=False)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Test mail JobOWL')

    # def test_uses_index_template(self):
    #     activate('en')
    #     response = self.client.get(reverse("home"))
    #     print response
    #     self.assertTemplateUsed(response, "jobmanager/index.html")