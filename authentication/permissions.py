from rest_framework import permissions


class IsAccountOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, account):
        if request.user:
            return account == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, task):
        if request.user.is_authenticated():
            return task == request.user.username
        return False