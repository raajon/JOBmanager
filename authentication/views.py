import json

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response
from django.utils import timezone
from rest_framework import permissions, viewsets
from django.contrib.auth import authenticate, login
from rest_framework import status, views
from rest_framework.response import Response
from authentication.models import Account
from authentication.permissions import IsAccountOwner
from authentication.serializers import AccountSerializer, ManageSerializer
from django.contrib.auth import logout
from rest_framework import permissions

class AccountViewSet(viewsets.ModelViewSet):
    lookup_field = 'username'
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        if self.request.method == 'POST':
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAccountOwner(),)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            Account.objects.create_user(**serializer.validated_data)
            return Response(serializer.validated_data, status=status.HTTP_201_CREATED)
        return Response({
            'status': 'Bad request',
            'message': 'Account could not be created with received data.'
        }, status=status.HTTP_400_BAD_REQUEST)

class LoginView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        data = json.loads(request.body)
        email = data.get('email', None)
        password = data.get('password', None)
        account = authenticate(email=email, password=password)
        if account is not None:
            if account.is_active:
                if account.is_enabled:
                    login(request, account)
                    serialized = AccountSerializer(account)
                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'You haven\' click link in the email'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

class LogoutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request, format=None):
        logout(request)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

class ManageViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = ManageSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        if self.request.method == 'POST':
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAccountOwner(),)

    def retrieve(self, request, pk=None, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(username=account_username).exclude(version=pk)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

class RemindView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        data = json.loads(request.body)
        email = data.get('email', None)
        try:
            account = Account.objects.get(email=email)
        except Account.DoesNotExist:
            account = None
        if account is not None:
            print "istnieje"
            Account.objects.remind_pass(account)
        else:
            print "brak"

        return Response({
            'status': 'OK',
            'message': 'Email sended'
        }, status=status.HTTP_200_OK)

class ChangePassView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        data = json.loads(request.body)
        key = data.get('key', None)
        password = data.get('pass', None)
        print key
        print password
        user_profile = get_object_or_404(Account, remind_key=key)
        if user_profile.key_expires < timezone.now():
            user_profile.remind_key = ""
            user_profile.save()
            return Response({
                'status': 'Error',
                'message': 'msg.passchange.expired'
            }, status=status.HTTP_400_BAD_REQUEST)
        user_profile.set_password(password)
        user_profile.remind_key = ""
        user_profile.save()
        return Response({
            'status': 'OK',
            'message': 'Email sended'
        }, status=status.HTTP_200_OK)

def register_confirm(request, activation_key):
    if request.user.is_authenticated():
        HttpResponseRedirect('/home')
    user_profile = get_object_or_404(Account, activation_key=activation_key)
    if user_profile.key_expires < timezone.now():
        user_profile.activation_key = ""
        user_profile.save()
        return render_to_response('index.html')
    user = user_profile
    user.activation_key = ""
    user.is_enabled = True
    user.save()
    return render_to_response('index.html')