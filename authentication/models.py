import hashlib
import random

import datetime
from django.contrib.auth.models import AbstractBaseUser
from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import BaseUserManager


class AccountManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError('Users must have a valid email address.')

        if not kwargs.get('username'):
            raise ValueError('Users must have a valid username.')

        account = self.model(
            email=self.normalize_email(email), username=kwargs.get('username')
        )

        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        account.activation_key = hashlib.sha1(salt + email).hexdigest()
        account.key_expires = datetime.datetime.today() + datetime.timedelta(2)

        link = 'http://jobowl.eu/pl/confirm/' + account.activation_key + '/'
        send_mail('Jobowl activation link', link, 'contact@jobowl.eu', [email], fail_silently=False)
        account.set_password(password)
        account.save()

        return account

    def create_superuser(self, email, password, **kwargs):
        account = self.create_user(email, password, **kwargs)

        account.is_admin = True
        account.save()

        return account

    def remind_pass(self, account):
        print account
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        account.remind_key = hashlib.sha1(salt + account.email).hexdigest()
        account.key_expires = datetime.datetime.today() + datetime.timedelta(2)
        link = 'http://jobowl.eu/pl/changepass/' + account.remind_key + '/'
        send_mail('Jobowl reset password link', link, 'contact@jobowl.eu', [account.email], fail_silently=False)
        account.save()

class Account(AbstractBaseUser):
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=40, unique=True)

    first_name = models.CharField(max_length=40, blank=True)
    last_name = models.CharField(max_length=40, blank=True)
    tagline = models.CharField(max_length=140, blank=True)

    is_admin = models.BooleanField(default=False)

    is_enabled = models.BooleanField(default=False)
    activation_key = models.CharField(max_length=40, blank=True, default="")
    remind_key = models.CharField(max_length=40, blank=True, default="")
    key_expires = models.DateTimeField(default=datetime.date.today())

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    version = models.IntegerField(default=0)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return self.email

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name])

    def get_short_name(self):
        return self.first_name

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin