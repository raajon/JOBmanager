from rest_framework import permissions, viewsets
from rest_framework.response import Response

from todos.models import Todo
from todos.permissions import IsAuthorOfTodo, IsAutorized
from todos.serializers import TodoSerializer, TodoReadSerializer


class TodoViewSet(viewsets.ModelViewSet):
    queryset = Todo.objects.order_by('-created_at')
    serializer_class = TodoSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfTodo(),)

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        return super(TodoViewSet, self).perform_create(serializer)



class AccountTodosViewSet(viewsets.ViewSet):
    queryset = Todo.objects.select_related('author').all()
    serializer_class = TodoReadSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username, done=False)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)


    def retrieve(self, request, pk=None, account_username=None):
        self.check_object_permissions(request, account_username)
        date=pk.split('-')
        if len(date) == 3:
            queryset = self.queryset.filter(author__username=account_username).filter(started_at__year=date[0],
                                                                   started_at__month=date[1],
                                                                   started_at__day=date[2])
        elif len(date) == 2:
            queryset = self.queryset.filter(author__username=account_username).filter(started_at__year=date[0],
                                                                                      started_at__month=date[1])
        elif len(date) == 1:
            queryset = self.queryset.filter(author__username=account_username).filter(day=date[0])
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
