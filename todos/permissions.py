from rest_framework import permissions


class IsAuthorOfTodo(permissions.BasePermission):
    def has_object_permission(self, request, view, todo):
        if request.user:
            return todo.author == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, todo):
        if request.user.is_authenticated():
            return todo == request.user.username
        return False