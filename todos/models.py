from django.db import models

from authentication.models import Account
from clients.models import Client
from employer.models import Employer
from projects.models import Project
from reportday.models import Day
from reportmonth.models import Month
from tasks.models import Task


class Todo(models.Model):
    author = models.ForeignKey(Account)
    employer = models.ForeignKey(Employer, null=True)
    project = models.ForeignKey(Project, null=True)
    task = models.ForeignKey(Task, null=True)
    client = models.ForeignKey(Client, null=True)
    descr = models.TextField(blank=True, null=True)
    prio = models.SmallIntegerField(default=0)
    expired_at = models.DateTimeField(null=True)
    done = models.BooleanField(default=False)

    def __unicode__(self):
        return '{0}'.format(self.content)