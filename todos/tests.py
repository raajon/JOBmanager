import datetime
from django.test import TestCase
from rest_framework.compat import RequestFactory
from rest_framework.test import APIClient

from authentication.models import Account
from tracks.models import Track


# class SimpleTest(TestCase):
#     def setUp(self):
#         # Every test needs access to the request factory.
#         self.factory = RequestFactory()
#         self.c = APIClient()
#         self.user1 = Account.objects.create_user(username='user1', email='user1@jomanager.pl', password='user1_pass')
#         self.user2 = Account.objects.create_user(username='user2', email='user2@jomanager.pl', password='user2_pass')
#
#         self.track1 = Track.objects.create(author_id=self.user1.id, started_at="2017-03-03")
#         self.track2 = Track.objects.create(author_id=self.user2.id, started_at="2017-03-03")
#
#     def test_details(self):
#         # Create an instance of a GET request.
#
#         response = self.c.get('/pl/api/v1/accounts/user1/tracks/')
#         self.assertEqual(response.status_code, 403)
#
#         self.c.force_authenticate(user=self.user1)
#
#         response = self.c.get('/pl/api/v1/accounts/user1/tracks/')
#         self.assertEqual(response.status_code, 200)
#
#         response = self.c.get('/pl/api/v1/accounts/user2/tracks/')
#         self.assertEqual(response.status_code, 403)
