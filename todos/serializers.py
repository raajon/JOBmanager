from rest_framework import serializers

from authentication.serializers import AccountSerializer
from clients.serializers import ClientSerializer
from employer.serializers import EmployerSerializer
from projects.serializers import ProjectSerializer
from tasks.serializers import TaskSerializer
from todos.models import Todo


class TodoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo

        fields = ('id', 'employer', 'project', 'task', 'client', 'descr', 'prio', 'expired_at', 'done')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(TodoSerializer, self).get_validation_exclusions()

        return exclusions + ['author']

class TodoReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo

        fields = ('id', 'employer', 'project', 'task', 'client', 'descr', 'prio', 'expired_at', 'done')
        depth = 1
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(TodoSerializer, self).get_validation_exclusions()

        return exclusions + ['author']