from rest_framework import permissions


class IsAuthorOfTrack(permissions.BasePermission):
    def has_object_permission(self, request, view, track):
        if request.user:
            return track.author == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, track):
        if request.user.is_authenticated():
            return track == request.user.username
        return False