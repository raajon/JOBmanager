from rest_framework import serializers

from authentication.serializers import AccountSerializer
from clients.serializers import ClientSerializer
from employer.serializers import EmployerSerializer
from projects.serializers import ProjectSerializer
from tasks.serializers import TaskSerializer
from tracks.models import Track


class TrackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Track

        fields = ('id', 'employer', 'project', 'task', 'client', 'descr', 'started_at', 'stoped_at')
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(TrackSerializer, self).get_validation_exclusions()

        return exclusions + ['author']

class TrackSerializerDay(serializers.ModelSerializer):
    employer =EmployerSerializer(read_only=True, required=False)
    project =ProjectSerializer(read_only=True, required=False)
    task =TaskSerializer(read_only=True, required=False)
    client =ClientSerializer(read_only=True, required=False)
    class Meta:
        model = Track

        fields = ('id', 'employer', 'project', 'task', 'client', 'descr', 'started_at', 'stoped_at')
        depth = 1
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(TrackSerializer, self).get_validation_exclusions()

        return exclusions + ['author']

