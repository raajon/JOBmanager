from rest_framework import permissions, viewsets
from rest_framework.response import Response

from projects.models import Project
from reportday.models import Day
from reportmonth.models import Month
from tracks.models import Track
from tracks.permissions import IsAuthorOfTrack, IsAutorized
from tracks.serializers import TrackSerializer


class TrackViewSet(viewsets.ModelViewSet):
    queryset = Track.objects.order_by('-created_at')
    serializer_class = TrackSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfTrack(),)

    def perform_create(self, serializer):
        day = self.request.data['day']
        oDay = Day.objects.get(pk=day)
        oMonth = Month.objects.get(pk=oDay.month.pk)
        oMonth.version = oMonth.version+1
        oMonth.save()
        if self.request.data.get('project'):
            project = self.request.data['project']
            oProject = Project.objects.get(pk=project)
            oProject.version = oProject.version + 1
            oProject.save()
        instance = serializer.save(author=self.request.user, day=oDay, month=oDay.month)
        return super(TrackViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        oTrack = Track.objects.get(pk=self.request.data['id'])
        instance = serializer.save()
        if instance.project:
            instance.project.version = instance.project.version +1
            instance.project.save()
        if oTrack.project and instance.project and oTrack.project.pk != instance.project.pk:
            oProject = Project.objects.get(pk=oTrack.project.pk)
            oProject.version = oProject.version + 1
            oProject.save()
        oMonth = Month.objects.get(pk=oTrack.month.pk)
        oMonth.version = oMonth.version+1
        oMonth.save()
        sum=0
        oTracks = Track.objects.filter(day=instance.day.id)
        for oTrack in oTracks:
            if oTrack.stoped_at:
                sum = sum + (oTrack.stoped_at - oTrack.started_at).total_seconds()
        instance.day.duration = int(sum)
        instance.day.save()

    def perform_destroy(self, instance):
        oTrack = Track.objects.get(pk=instance.pk)
        oMonth = Month.objects.get(pk=oTrack.month.pk)
        oMonth.version = oMonth.version + 1
        oMonth.save()
        if oTrack.project:
            oProject = Project.objects.get(pk=oTrack.project.pk)
            oProject.version = oProject.version + 1
            oProject.save()
        instance.delete()

class AccountTracksViewSet(viewsets.ViewSet):
    queryset = Track.objects.select_related('author').all()
    serializer_class = TrackSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)


    def retrieve(self, request, pk=None, account_username=None):
        self.check_object_permissions(request, account_username)
        date=pk.split('-')
        if len(date) == 3:
            queryset = self.queryset.filter(author__username=account_username).filter(started_at__year=date[0],
                                                                   started_at__month=date[1],
                                                                   started_at__day=date[2])
        elif len(date) == 2:
            queryset = self.queryset.filter(author__username=account_username).filter(started_at__year=date[0],
                                                                                      started_at__month=date[1])
        elif len(date) == 1:
            queryset = self.queryset.filter(author__username=account_username).filter(day=date[0])
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
