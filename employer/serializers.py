from rest_framework import serializers

from employer.models import Employer


class EmployerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employer

        fields = ('id', 'name', 'default', 'project_set', 'done')
        depth = 0
        read_only_fields = ('id')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(EmployerSerializer, self).get_validation_exclusions()

        return exclusions + ['author']