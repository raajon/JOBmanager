from rest_framework import permissions, viewsets
from rest_framework.response import Response

from employer.models import Employer
from employer.permissions import IsAutorized, IsAuthorOfEmployer
from employer.serializers import EmployerSerializer


class EmployerViewSet(viewsets.ModelViewSet):
    queryset = Employer.objects.order_by('-created_at')
    serializer_class = EmployerSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsAuthorOfEmployer(),)

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        instance.author.version = instance.author.version+1
        instance.author.save()
        return super(EmployerViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = serializer.save()
        print instance.author.version
        instance.author.version = instance.author.version+1
        instance.author.save()

class AccountEmployersViewSet(viewsets.ViewSet):
    queryset = Employer.objects.select_related('author').all()
    serializer_class = EmployerSerializer
    permission_classes = (IsAutorized,)

    def list(self, request, account_username=None):
        self.check_object_permissions(request, account_username)
        queryset = self.queryset.filter(author__username=account_username, done=False)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)
