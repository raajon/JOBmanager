from rest_framework import permissions


class IsAuthorOfEmployer(permissions.BasePermission):
    def has_object_permission(self, request, view, employer):
        if request.user:
            return employer.author == request.user
        return False

class IsAutorized(permissions.BasePermission):

    def has_object_permission(self, request, view, task):
        if request.user.is_authenticated():
            return task == request.user.username
        return False