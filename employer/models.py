from django.db import models

from authentication.models import Account


class Employer(models.Model):
    author = models.ForeignKey(Account)
    name = models.CharField(max_length=80, blank=True)
    default = models.BooleanField(default=False)
    done = models.BooleanField(default=False)

    def __unicode__(self):
        return '{0}'.format(self.content)